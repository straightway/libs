/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.collections

import org.junit.jupiter.api.Test
import straightway.assertions.assertThat
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.bdd.Given
import straightway.bdd.isNull

class TypedMapTest {

    private val test get() = Given {
        object {
            val key1 = TypedMapKey<String>("key1")
            val key2 = TypedMapKey<Int>("key2")
            val notExistingKey = TypedMapKey<Boolean>("not existing key")
            val sut = typedMapOf(
                key1 to "Hello",
                key2 to 83
            )
        }
    }

    @Test
    fun `get returns proper value`() =
        test when_ {
            sut[key1]
        } then {
            assertThat(it is_ equal to "Hello")
        }

    @Test
    fun `get for not existing key returns null`() =
        test when_ {
            sut[notExistingKey]
        } then isNull

    @Test
    fun `contains returns true if key is contained`() =
        test when_ {
            key2 in sut
        } then {
            assertThat(it is_ true)
        }

    @Test
    fun `contains returns false if key is not contained`() =
        test when_ {
            notExistingKey in sut
        } then {
            assertThat(it is_ false)
        }

    @Test
    fun `size yields proper value`() =
        test when_ {
            sut.size
        } then {
            assertThat(it is_ equal to 2)
        }

    @Test
    fun `is iterable`() =
        test when_ {
            sut.toList()
        } then {
            assertThat(it is_ equal to listOf(key1 to "Hello", key2 to 83))
        }
}
