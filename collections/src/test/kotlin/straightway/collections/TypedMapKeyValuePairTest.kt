/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.collections

import org.junit.jupiter.api.Test
import straightway.assertions.assertThat
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.bdd.Given

class TypedMapKeyValuePairTest {

    private val test get() = Given {
        TypedMapKeyValuePair(TypedMapKey("key"), "value")
    }

    @Test
    fun `key is accessible`() =
        test when_ {
            key
        } then {
            assertThat(it is_ equal to TypedMapKey<String>("key"))
        }

    @Test
    fun `value is accessible`() =
        test when_ {
            value
        } then {
            assertThat(it is_ equal to "value")
        }
}
