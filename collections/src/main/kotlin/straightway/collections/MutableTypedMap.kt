/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.collections

/**
 * A mutable map with string key and type safe access to individual value types per key.
 */
interface MutableTypedMap : TypedMap {
    operator fun <T : Any> set(key: TypedMapKey<T>, value: T)

    fun <T : Any> remove(key: TypedMapKey<T>): T?
}

/**
 * Construct a new mutable typed map of key value pairs.
 * @param values The key value pairs for the map.
 * @return The new MutableTypedMap
 */
fun mutableTypedMapOf(vararg values: TypedMapKeyValuePair<*>): MutableTypedMap =
    MutableTypedMapImpl(values.associate { it.key.key to it.value }.toMutableMap())

private data class MutableTypedMapImpl(private val map: MutableMap<String, Any>) : MutableTypedMap {

    override val size get() = map.size

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> get(key: TypedMapKey<T>): T? = map[key.key]?.let { it as T }

    override fun <T : Any> set(key: TypedMapKey<T>, value: T) {
        map[key.key] = value
    }

    override fun <T : Any> contains(key: TypedMapKey<T>): Boolean = map.containsKey(key.key)

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> remove(key: TypedMapKey<T>): T? = map.remove(key.key)?.let { it as T }

    override fun iterator(): Iterator<TypedMapKeyValuePair<*>> =
        map.map { TypedMapKeyValuePair(TypedMapKey(it.key), it.value) }.iterator()
}
