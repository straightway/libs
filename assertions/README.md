# assertions

This kotlin library makes it more convenient to write unit tests using JUnit 5 in kotlin.
It provides a flow syntax interface for expectations (similar to NUnits Expect syntax), which
is far better readable than the JUnit asserts. These asserts generate human-readable output in
case of a failure. And they "normalize" the comparison of containers and arrays.

## Example

    assertThat(it is_ not - equal to "Hello")
    assertThat(it has size of 7)

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:assertions:<version>"
    }
