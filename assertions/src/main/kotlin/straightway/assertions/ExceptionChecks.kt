/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.assertions

import org.junit.jupiter.api.Assertions

/**
 * Check the given action for exceptions and optionally perform specific actions
 * on success and on failure.
 * @param TResult The result type of the action
 * @param onSuccess The action to execute on success.
 * @param onFailure The action to execute on failure. If the result is null, exceptions thrown in
 * the action result in assertion failures. If it is not null, this result is returned when an
 * exception is thrown.
 * @return The action result or the onFailure result.
 */
fun <TResult> checkActionForException(
    onSuccess: () -> Unit = {},
    onFailure: (Throwable) -> TResult? = { null },
    action: () -> TResult
): TResult =
    try {
        action().apply { onSuccess() }
    } catch (e: Throwable) {
        val result = onFailure(e)
        if (result !== null) result
        else Assertions.fail("Unexpected exception: $e")
    }

/**
 * Check if a given exception is as expected.
 * @param TException The expected exception type
 * @param expectedMessage The expected exception message.
 */
inline fun <reified TException : Throwable> Throwable.isExpectedException(
    noinline action: () -> Unit,
    expectedMessage: String
) {
    if (this !is TException)
        failDueToWrongExceptionType(action)
    Assertions.assertEquals(expectedMessage, message)
}

/**
 * Check if a given exception is as expected.
 * @param TException The expected exception type
 * @param expectedMessageRegex A regex expected to match the exception message.
 */
inline fun <reified TException : Throwable> Throwable.isExpectedException(
    noinline action: () -> Unit,
    expectedMessageRegex: Regex
) {
    if (this !is TException)
        failDueToWrongExceptionType(action)
    if (!(expectedMessageRegex.matches(message!!)))
        org.junit.jupiter.api.fail(
            "Unexpected failure message: $message " +
                "(expected pattern: $expectedMessageRegex)"
        )
}

// region private

fun Throwable.failDueToWrongExceptionType(action: () -> Unit) {
    org.junit.jupiter.api.fail("Action $action threw unexpected exception $this")
}

fun failDueToMissingException(action: () -> Unit) {
    org.junit.jupiter.api.fail("Action $action did not throw an exception")
}

// endregion
