/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.assertions

import org.junit.jupiter.api.Assertions
import straightway.error.Panic
import straightway.expr.Expr
import straightway.expr.StateExpr

/**
 * Expect the condition evaluated by the given expression to be true.
 * @param condition The condition which must be true
 * @param messageGenerator Generates an error message if the condition is not met.
 * @throws AssertionError When the condition is not met.
 */
fun assertThat(condition: Expr, messageGenerator: (Expr) -> String = { "" }): Unit =
    checkActionForException(
        onFailure = {
            condition.handleFailure(it, messageGenerator)
        }
    ) {
        try {
            check(condition)
        } catch (e: Panic) {
            condition.failedExpectationWith(e.state) { "" }
        }
    }

/**
 * Expect the condition evaluated by the given expression to be true.
 * @param condition The condition which must be true
 * @param messageGenerator Generates an error message if the condition is not met.
 * @throws AssertionError When the condition is not met.
 */
fun assertThat(
    condition: Pair<StateExpr<WithTo>, Any?>,
    messageGenerator: (Expr) -> String = { "" }
): Unit =
    assertThat(condition.first to_ condition.second, messageGenerator)

/**
 * Expect the condition evaluated by the given expression to be true.
 * @param condition The condition which must be true
 * @param messageGenerator Generates an error message if the condition is not met.
 * @throws AssertionError When the condition is not met.
 */
fun assertThat(condition: Boolean, messageGenerator: (Expr) -> String = { "" }): Unit =
    assertThat(condition is_ true, messageGenerator)

/**
 * Generate a failure with the give message.
 * @throws AssertionError This exception is thrown to indicate the failure.
 */
fun fail(message: String) { Assertions.fail<Unit>(message) }

/**
 * Assert that the given action throws a Panic exception.
 * @param action The action to check for Panic.
 * @throws AssertionError When the action does not throw a Panic.
 */
fun assertPanics(action: () -> Unit) {
    assertThrows<Panic>(action)
}

/**
 * Assert that the given action throws a Panic exception with the given state.
 * @param expectedState The expected state of the thrown Panic exception.
 * @param action The action to check for Panic.
 * @throws AssertionError When the action does not throw a Panic.
 */
fun assertPanics(expectedState: Any, action: () -> Unit) {
    try {
        action()
        Assertions.fail<Unit>("Action $action did not cause panic")
    } catch (panic: Panic) {
        Assertions.assertEquals(expectedState, panic.state)
    }
}

/**
 * Assert that the given action throws an AssertionError.
 * @param action The action to check for an exception.
 * @throws AssertionError When the action does not throw an AssertionError.
 */
fun assertFails(action: () -> Unit) {
    assertThrows<AssertionError>(action)
}

/**
 * Assert that the given action throws an AssertionError with the specified message.
 * @param expectedMessage The message which is expected with the AssertionError.
 * @param action The action to check for an exception.
 * @throws AssertionError When the action does not throw an AssertionError.
 */
fun assertFails(expectedMessage: String, action: () -> Unit) {
    assertThrows<AssertionError>(expectedMessage, action)
}

/**
 * Assert that the given action throws an AssertionError with the specified message.
 * @param expectedMessage A regular expression which is expected to match the message of
 * the AssertionError.
 * @param action The action to check for an exception.
 * @throws AssertionError When the action does not throw an AssertionError.
 */
fun assertFails(expectedMessage: Regex, action: () -> Unit) {
    assertThrows<AssertionError>(expectedMessage, action)
}

/**
 * Assert that the given action does not throw any exception.
 * @param action The action to check for an exception.
 * @throws AssertionError When the action does throw an exception.
 */
fun assertDoesNotThrow(action: () -> Unit) {
    checkActionForException(
        action = action,
        onSuccess = {},
        onFailure = {
            Assertions.fail<Unit>(
                "Action $action threw unexpected exception $it\n${it.stackTraceToString()}"
            )
        }
    )
}

/**
 * Assert that the given action throws the specified exception.
 * @param TException The expected exception type.
 * @param action The action to check for an exception.
 * @throws AssertionError When the action does not throw the specified exception.
 */
inline fun <reified TException : Throwable> assertThrows(noinline action: () -> Unit) {
    Assertions.assertThrows(TException::class.java, action)
}

/**
 * Assert that the given action throws the specified exception with a specific message.
 * @param TException The expected exception type.
 * @param expectedMessageRegex The regular expression that is expected to match the
 * exception message.
 * @param action The action to check for an exception.
 * @throws AssertionError When the action does not throw the specified exception.
 */
inline fun <reified TException : Throwable> assertThrows(
    expectedMessageRegex: Regex,
    noinline action: () -> Unit
) {
    checkActionForException(
        action = action,
        onSuccess = {
            failDueToMissingException(action)
        },
        onFailure = {
            it.isExpectedException<TException>(action, expectedMessageRegex)
        }
    )
}

/**
 * Assert that the given action throws the specified exception with a specific message.
 * @param TException The expected exception type.
 * @param expectedMessage The expected exception message.
 * @param action The action to check for an exception.
 * @throws AssertionError When the action does not throw the specified exception.
 */
inline fun <reified TException : Throwable> assertThrows(
    expectedMessage: String,
    noinline action: () -> Unit
) {
    checkActionForException(
        action = action,
        onSuccess = {
            failDueToMissingException(action)
        },
        onFailure = {
            it.isExpectedException<TException>(action, expectedMessage)
        }
    )
}

/**
 * Assert that the receiver object has the specified type and, if this is the case
 * execute the given action on that object.
 * @param T The expected receiver type.
 * @param typeAction The action to execute on the receiver object.
 * @throws AssertionError When the receiver type is not as expected.
 */
inline fun <reified T> Any.assertType(typeAction: T.() -> Unit) {
    if (this !is T)
        org.junit.jupiter.api.fail("Expected: ${T::class}, got: $this")
    typeAction(this)
}

// region Private

private fun Expr.failedExpectationWith(info: Any, messageGenerator: (Expr) -> String) {
    val message = "Expectation <${ExpressionVisualizer(this).string}> failed ($info)"
    fail(getCombinedMessage(message, messageGenerator))
}

private fun Expr.getCombinedMessage(
    message: String,
    messageGenerator: (Expr) -> String
): String {
    val generatedMessage = messageGenerator(this)
    return if (generatedMessage.isEmpty())
        message
    else "$generatedMessage ($message)"
}

private fun check(condition: Expr) =
    with(condition() as AssertionResult) {
        if (!isSuccessful) fail("Expectation $explanation failed.")
    }

private fun Expr.handleFailure(exception: Throwable, messageGenerator: (Expr) -> String) {
    if (exception is AssertionError)
        throw AssertionError(getCombinedMessage(exception.message ?: "", messageGenerator))
    failedExpectationWith(exception, messageGenerator)
}

// endregion
