/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.assertions

import kotlinx.coroutines.*
import straightway.expr.BoundExpr
import straightway.expr.Expr
import straightway.expr.StateExpr
import straightway.expr.Value
import straightway.expr.inState

/**
 * An expression which tests the effect of a given lambda object.
 */
interface Effect : Expr

/**
 * Flow syntax operator checking if the blocking receiver action has a given effect.
 * @param T The effect type to check.
 * @param effect The effect which is expected from the receiver action.
 */
infix fun <T : Effect> (() -> Any?).does(effect: StateExpr<T>) =
    BoundExpr(effect, Value(this)).inState<T>()

/**
 * Flow syntax operator checking if the suspending receiver action has a given effect.
 * @param T The effect type to check.
 * @param effect The effect which is expected from the receiver action.
 */
infix fun <T : Effect> (suspend () -> Any?).does(effect: StateExpr<T>) = let { func ->
    { runBlocking { func() } } does effect
}
