/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.assertions

import straightway.expr.FunExpr
import straightway.expr.StateExpr
import java.io.File
import java.nio.file.Path

/**
 * Checks for a file or a path whether it exists or not.s
 */
class IsFileExisting :
    Relation,
    StateExpr<WithHas>,
    FunExpr(
        "is file existing",
        { a: Any? ->
            val f = if (a is File) a else if (a is Path) a.toFile() else null
            if (f === null)
                AssertionResult.failure("$a is neither a file nor a path")
            else AssertionResult("file $a exists", f.exists())
        }
    )

val existing = IsFileExisting()
