/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
module straightway.assertions {
    requires kotlin.stdlib;
    requires straightway.error;
    requires straightway.expr;
    requires org.junit.jupiter.api;
    requires straightway.stringformat;
    requires straightway.numbers;
    requires kotlinx.coroutines.core.jvm;
    exports straightway.assertions;
}