/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.assertions

import org.junit.jupiter.api.Test
import straightway.expr.Value
import straightway.expr.minus

class AssertTest {

    @Test
    fun assertionResultExpression_notSuccessful_isFailure() =
        assertFails { assertThat(Value(AssertionResult("Explanation", false))) }

    @Test
    fun nonAssertionResultExpression_isFailure() =
        assertFails(
            Regex(
                ".*Expectation <1> failed.*java.lang.ClassCastException.*" +
                    "java\\.lang\\.Integer cannot be cast to .*" +
                    "straightway\\.assertions\\.AssertionResult.*"
            )
        ) {
            assertThat(Value(1))
        }

    @Test
    fun assertionResultExpression_notSuccessful_isSuccess() =
        assertDoesNotThrow { assertThat(Value(AssertionResult("Explanation", true))) }

    @Test
    fun failure_singleDyadicOp_withMeaningfulExplanation() =
        assertFails(Regex("Expectation 1 > 2 failed.*")) {
            assertThat(1 is_ greater than 2)
        }

    @Test
    fun failure_monadicWithDyadicOp_withMeaningfulExplanation() =
        assertFails(Regex(".*Expectation not 1 == 1 failed.*")) {
            assertThat(1 is_ not - equal to_ 1)
        }

    @Test
    fun failure_notFullyBoundExpression_withMeaningfulExplanation() =
        assertFails(
            "Expectation <1 Greater ?> failed " +
                "(Invalid number of parameters. Expected: 2, got: 1)"
        ) {
            assertThat(1 is_ greater)
        }

    @Test
    fun `failure with AssertionResult expression does not show Expected and Actual in message`() =
        assertFails("Expectation Explanation failed.") {
            assertThat(Value(AssertionResult("Explanation", false)))
        }

    @Test
    fun `assertThat with false condition, error message function and message generator fails`() =
        assertFails("ErrorMessage (Expectation Explanation failed.)") {
            assertThat(Value(AssertionResult("Explanation", false))) {
                "ErrorMessage"
            }
        }

    @Test
    fun success_directlyUsingBoolean() =
        assertDoesNotThrow { assert(true) }

    @Test
    fun failure_directlyUsingBoolean() =
        assertFails { assert(false) }

    @Test
    fun success_directlyUsingBoolean_withExplanation() =
        assertDoesNotThrow { assert(true) { "Explanation" } }

    @Test
    fun failure_directlyUsingBoolean_withExplanation() =
        assertFails(Regex("Explanation.*")) { assert(false) { "Explanation" } }

    @Test
    fun `assertThat with true condition succeeds`() =
        assertDoesNotThrow { assertThat(true) }

    @Test
    fun `assertThat with false condition fails`() =
        assertFails { assertThat(false) }

    @Test
    fun `assertThat with false condition and error message function fails`() =
        assertFails(Regex("ErrorMessage \\(.*\\)")) {
            assertThat(false) { "ErrorMessage" }
        }
}
