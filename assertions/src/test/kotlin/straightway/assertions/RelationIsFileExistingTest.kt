/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.assertions

import org.junit.jupiter.api.Test
import straightway.bdd.Given
import straightway.bdd.fails
import straightway.bdd.throws
import straightway.files.div
import straightway.files.doWithFile
import java.nio.file.Files

class RelationIsFileExistingTest {
    private val test get() = Given {
        object : AutoCloseable {
            val baseDir = Files
                .createTempDirectory("AppsModelTest").toFile()

            override fun close() {
                baseDir.deleteRecursively()
            }
        }
    }

    @Test
    fun `not existing file does not exist`() =
        test when_ {
            assertThat(baseDir / "notExistingFile" is_ existing)
        } then fails {
            assertThat(
                it.message is_ equal to
                    "Expectation file ${baseDir / "notExistingFile"} exists failed."
            )
        }

    @Test
    fun `existence check for other class than File and Path fail`() =
        test when_ {
            assertThat("NonPathAndFile" is_ existing)
        } then fails {
            assertThat(
                it.message is_ equal to
                    "Expectation NonPathAndFile is neither a file nor a path failed."
            )
        }

    @Test
    fun `existing file does exist`() =
        test while_ {
            baseDir / "file.txt" doWithFile { writeText("Lalala") }
        } when_ {
            assertThat(baseDir / "file.txt" is_ existing)
        } then throws.nothing

    @Test
    fun `existence check for Path object succeeds`() =
        test while_ {
            baseDir / "file.txt" doWithFile { writeText("Lalala") }
        } when_ {
            assertThat(baseDir.toPath() / "file.txt" is_ existing)
        } then throws.nothing
}
