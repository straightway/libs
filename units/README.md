# units

This kotlin library allows using numerical values with units like meter, kelvin or kilogram, including conversion and combination of units (e.g. meter/second).

To have a number with an attached unit, use the index operator,
e.g.

    val temperature = 273[kelvin]
    val speed = 120[kilo(meter) / hour]

For more samples, see the unit tests.

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:units:<version>"
    }