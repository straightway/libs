/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.units

import straightway.numbers.minus
import straightway.numbers.times
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

/**
 * Magnitude for time.
 */
class Time constructor(
    symbol: String,
    scale: UnitScale,
    baseMagnitude: Number
) : QuantityBase(symbol, scale, baseMagnitude, { Time(symbol, it, baseMagnitude) }) {
    constructor(symbol: String, numberOfSeconds: Number) : this(symbol, uni, numberOfSeconds)

    override val baseQuantity: Quantity get() = second
    override fun isProperlyScaledWith(magnitude: Number) = magnitude in predefinedMagnitudes

    internal companion object {

        const val SECONDS_PER_MINUTE = 60
        const val SECONDS_PER_HOUR = 60 * SECONDS_PER_MINUTE
        const val SECONDS_PER_DAY = 24 * SECONDS_PER_HOUR
        const val SECONDS_PER_WEEK = 7 * SECONDS_PER_DAY
        const val SECONDS_PER_YEAR = 31558432.5504

        private val predefinedMagnitudes = setOf<Number>(
            SECONDS_PER_MINUTE,
            SECONDS_PER_HOUR,
            SECONDS_PER_DAY,
            SECONDS_PER_WEEK,
            SECONDS_PER_YEAR
        )
    }
}

const val NANO_FACTOR = 1_000_000_000
val second = Time("s", 1)
val minute = Time("min", Time.SECONDS_PER_MINUTE)
val hour = Time("h", Time.SECONDS_PER_HOUR)
val day = Time("d", Time.SECONDS_PER_DAY)
val week = Time("wk", Time.SECONDS_PER_WEEK)
val year = Time("a", Time.SECONDS_PER_YEAR)

fun UnitValue<Time>.toDuration(): Duration {
    val seconds = this.baseValue.toLong()
    val nanos = ((this.baseValue - seconds) * NANO_FACTOR).toLong()
    return Duration.ofSeconds(seconds, nanos)
}

operator fun LocalDateTime.plus(amount: UnitValue<Time>) =
    (this + amount.toDuration())!!

operator fun LocalDateTime.minus(amount: UnitValue<Time>) =
    (this - amount.toDuration())!!

operator fun LocalDateTime.minus(other: LocalDateTime): UnitDouble<Time> =
    Duration.between(other, this).toTime()

fun Duration.toTime() = this.nano.toDouble()[nano(second)] + this.seconds.toDouble()[second]

private val zeroTime = LocalDateTime.of(0, 1, 1, 0, 0)
val UnitValue<Time>.absolute get() = zeroTime + this
val LocalDateTime.unitValue: UnitDouble<Time> get() = this - zeroTime

fun LocalDate.at(time: UnitValue<Time>) = LocalDateTime.of(this, LocalTime.MIDNIGHT) + time
