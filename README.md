# Straightway libs

The straightway libs are a collection of useful libraries for various purposes.

## Released

* [assertions](assertions/README.md): Provides assertion in flow syntax with enhanced readability and human-readable error output.
* [bdd](bdd/README.md): Support for behaviour driven development style tests (BDD, Given-When-Then schema).
* [binaryconversion](binaryconversion/README.md): Convert numbers and strings as well as serializable objects into byte arrays, and vice versa.
* [collections](collections/README.md): Specific collection classes and helper methods for collections.
* [comparison](comparison/README.md): Algorithms on comparable objects.
* [compiler](compiler/README.md): Fine tuning how the compiler and tools and plugins operate on the source code.
* [error](error/README.md): Error handling and propagation utilities.
* [events](events/README.md): A simple event mechanism allowing to separate attaching/detaching from firing the events.
* [expr](expr/README.md): Partially bound functional expressions with a nice string representation.
* [files](files/README.md): Create file paths using the div operator, which enhances the readability of the path specification in the source code.
* [lifecycle](lifecycle/README.md): Utilities for handling object life cycles.
* [numbers](numbers/README.md): Extension methods and operators for calculation with mixed number types without the necessity to cast.
* [service](service/README.md): Load and instantiate services from java modules.
* [statemachine](statemachine/README.md): A generic state machine, which can be configured with custom states, custom events and custom transitions.
* [stringformat](stringformat/README.md): Converting and formatting strings.
* [time](time/README.md): Provides the TimeProvider interface for abstracting time dependent code from the real time.

## Pre-Release

* [aoplite](aoplite/README.md): Supports a lightweight way of AOP style programming, without compiler magic, just plain kotlin code.
* [crypto](crypto/README.md): Convenience for symmetric and asymmetric cryptography and hashing.
* [random](random/README.md): Easy and flexible handling of random numbers with a kotlin specific API.
* [ranges](ranges/README.md): Basic range operations (union, difference, intersection, containment check), as well as a class containing a set of disjoint ranges and a comparator for ranges (for storing them in an ordered list).
* [tracing](tracing/README.md): Add tracing to functions.
* [units](units/README.md): Numerical values with units
  like meter, kelvin or kilogram, including conversion and combination of units (e.g. meter/second).

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:<lib>:<version>"
    }
