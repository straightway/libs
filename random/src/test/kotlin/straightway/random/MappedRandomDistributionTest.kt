/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.random

import org.junit.jupiter.api.Test
import org.mockito.kotlin.mock
import straightway.assertions.assertThat
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.assertions.to_
import straightway.bdd.Given

class MappedRandomDistributionTest {

    @Test
    fun `values are mapped`() =
        Given {
            val numbers = arrayOf(1, 2, 3).iterator()
            val underlyingDistribution = mock<RandomDistribution<Int>> {
                onGeneric { hasNext() }.thenAnswer { numbers.hasNext() }
                onGeneric { next() }.thenAnswer { numbers.next() }
            }
            MappedRandomDistribution(underlyingDistribution) { "i${-it}" }
        } when_ {
            joinToString(",")
        } then {
            assertThat(it is_ equal to_ "i-1,i-2,i-3")
        }
}
