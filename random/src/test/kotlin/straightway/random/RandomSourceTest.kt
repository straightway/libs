/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.random

import org.junit.jupiter.api.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import straightway.assertions.assertThat
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.assertions.to_
import straightway.assertions.values
import straightway.bdd.Given

class RandomSourceTest {

    private val test get() = Given {
        object {
            private val numbers = byteArrayOf(1, 2, 3, 4, 5)
            private var currNumber = 0
            val random = mock<RandomGenerator> {
                on { nextBytes(any()) }.thenAnswer { invocation ->
                    val outBuffer = invocation.arguments[0] as ByteArray
                    outBuffer.indices.forEach {
                        outBuffer[it] = numbers[currNumber]
                        currNumber = (currNumber + 1) % numbers.size
                    }
                }
            }
            val sut = RandomSource(random)
        }
    }

    @Test
    fun `hasNext is true`() =
        test when_ { sut.hasNext() } then { assertThat(it is_ true) }

    @Test
    fun `next gets next byte from generator specified in construction`() =
        test when_ { sut.next() } then {
            verify(random).nextBytes(any())
        }

    @Test
    fun `can be used with LINQ like expressions`() =
        test when_ { sut.take(2) } then {
            assertThat(it is_ equal to_ values(1.toByte(), 2.toByte()))
        }
}
