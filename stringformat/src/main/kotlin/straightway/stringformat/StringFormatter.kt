/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.stringformat

import java.nio.file.Path

/**
 * Get a 'reasonably' formatted string for the given receiver type.
 */
fun Any?.getFormatted(): String = when (this) {
    is String -> "\"$this\""
    is Byte -> this.toHex()
    is Path -> this.toString()
    is Array<*> -> this.toList().getFormatted()
    is ByteArray -> this.toList().getFormatted()
    is CharArray -> this.toList().getFormatted()
    is ShortArray -> this.toList().getFormatted()
    is IntArray -> this.toList().getFormatted()
    is LongArray -> this.toList().getFormatted()
    is FloatArray -> this.toList().getFormatted()
    is DoubleArray -> this.toList().getFormatted()
    is BooleanArray -> this.toList().getFormatted()
    is ClosedRange<*> -> "${this.start.getFormatted()}..${this.endInclusive.getFormatted()}"
    is Iterable<*> -> iterableFormatted()
    is Map<*, *> ->
        "{" +
            this.entries.joinToString(", ") {
                it.key.getFormatted() + "=" + it.value.getFormatted()
            } +
            "}"
    else -> if (this === null) "<null>" else toString()
}

/**
 * Get a string which re-formats the lines of the receiver string in order to get as long
 * lines as possible.
 */
fun String.reformattedLines(): String {
    var isPreviousLineJoinable = true
    return lines().joinToString("") {
        val trimmed = it.trim()
        val isThisLineJoinable = isPreviousLineJoinable &&
            JOINABLE_LINE_START_REGEX.matches(trimmed)
        isPreviousLineJoinable = JOINABLE_LINE_END_REGEX.matches(trimmed)
        if (isThisLineJoinable)
            " $trimmed"
        else "\n$trimmed"
    }.trim()
}

// region Private

private const val MAX_FULL_SIZE = 32
private const val ELEMENTS_TO_CUT = MAX_FULL_SIZE / 2
private val JOINABLE_LINE_START_REGEX = Regex("^[a-zA-Z0-9.,;:?!(/\"'].*$")
private val JOINABLE_LINE_END_REGEX = Regex("^.*[a-zA-Z0-9.,;:?!)/\"']$")

private fun Iterable<*>.iterableFormatted(): String {
    val asList = toList()
    return if (asList.size <= MAX_FULL_SIZE) asList.map { it.getFormatted() }.toString()
    else (
        asList.slice(0 until ELEMENTS_TO_CUT).map { it.getFormatted() } +
            listOf("...(${asList.size - MAX_FULL_SIZE} more)...") +
            asList.slice((asList.size - ELEMENTS_TO_CUT)..asList.lastIndex)
                .map { it.getFormatted() }
        ).toString()
}

// endregion
