/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.stringformat

/**
 * Join the string representations of the items of receiver object to a multi-line string,
 * each item on its own line. The whole result is enclosed in braces.
 * @param indentation The number of spaces to indent the item lines.
 */
fun Iterable<*>.joinMultiLine(indentation: Int = 0): String =
    with(joinToString("\n") { "$it".getIndented(indentation) }) {
        if (isEmpty()) "{}" else "{\n$this\n}"
    }

/**
 * Returns the reciever multi line string indented by the given number of spaces.
 * @param indentation The number of spaces to indent the receiver string.
 */
fun String.getIndented(indentation: Int): String =
    " ".repeat(indentation).let { indent ->
        lines().joinToString("\n") { indent + it }
    }
