/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.stringformat

import org.junit.jupiter.api.Test
import straightway.assertions.assertThat
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.assertions.to_
import straightway.assertions.values
import straightway.bdd.Given
import java.nio.file.Path

class StringFormatterTest {

    @Test
    fun `for normal object, toString is called`() =
        assertThat(3.getFormatted() is_ equal to_ "3")

    @Test
    fun `for byte, hex string is returned`() =
        assertThat(16.toByte().getFormatted() is_ equal to_ "10")

    @Test
    fun `null value yields null string representation`() =
        assertThat(null.getFormatted() is_ equal to_ "<null>")

    @Test
    fun `collection yields string with elements in bracked`() =
        assertThat(listOf(1, 2, 3).getFormatted() is_ equal to_ "[1, 2, 3]")

    @Test
    fun `array yields string with elements in bracked`() =
        assertThat(arrayOf(1, 2, 3).getFormatted() is_ equal to_ "[1, 2, 3]")

    @Test
    fun `ByteArray yields string with hex elements in bracked`() =
        assertThat(byteArrayOf(1, 2, -1).getFormatted() is_ equal to_ "[01, 02, ff]")

    @Test
    fun `CharArray yields string with elements in bracked`() =
        assertThat(charArrayOf('1', '2', '3').getFormatted() is_ equal to_ "[1, 2, 3]")

    @Test
    fun `ShortArray yields string with elements in bracked`() =
        assertThat(shortArrayOf(1, 2, 3).getFormatted() is_ equal to_ "[1, 2, 3]")

    @Test
    fun `IntArray yields string with elements in bracked`() =
        assertThat(intArrayOf(1, 2, 3).getFormatted() is_ equal to_ "[1, 2, 3]")

    @Test
    fun `LongArray yields string with elements in bracked`() =
        assertThat(longArrayOf(1, 2, 3).getFormatted() is_ equal to_ "[1, 2, 3]")

    @Test
    fun `FloatArray yields string with elements in bracked`() =
        assertThat(floatArrayOf(1.0F, 2.0F, 3.0F).getFormatted() is_ equal to_ "[1.0, 2.0, 3.0]")

    @Test
    fun `DoubleArray yields string with elements in bracked`() =
        assertThat(doubleArrayOf(1.0, 2.0, 3.0).getFormatted() is_ equal to_ "[1.0, 2.0, 3.0]")

    @Test
    fun `BooleanArray yields string with elements in bracked`() =
        assertThat(booleanArrayOf(true, false).getFormatted() is_ equal to_ "[true, false]")

    @Test
    fun `collection of arrays formats its elements properly`() =
        assertThat(listOf(arrayOf(1)).getFormatted() is_ equal to_ "[[1]]")

    @Test
    fun `map with array as key formats its elements properly`() =
        assertThat(
            mapOf(arrayOf(1) to 2, arrayOf(3) to 4).getFormatted()
                is_ equal to_ "{[1]=2, [3]=4}"
        )

    @Test
    fun `map with array as value formats its elements properly`() =
        assertThat(
            mapOf(2 to arrayOf(1), 4 to arrayOf(3)).getFormatted()
                is_ equal to_ "{2=[1], 4=[3]}"
        )

    @Test
    fun `string is formatted with quotes`() =
        assertThat("Hello".getFormatted() is_ equal to_ "\"Hello\"")

    @Test
    fun `LongRange is formatted with borders`() =
        assertThat((1L..5L).getFormatted() is_ equal to_ "1..5")

    @Test
    fun `Values with array inside is properly formatted`() =
        assertThat(
            values(byteArrayOf(1, 2), byteArrayOf(3)).getFormatted() is_ equal
                to_ "Values[[01, 02], [03]]"
        )

    @Test
    fun `large arrays are cut in the middle`() =
        assertThat(
            IntArray(300) { it }.getFormatted() is_ equal
                to_ "[${(0..15).joinToString(", ")}, ...(268 more)..., " +
                    "${(284..299).joinToString(", ")}]"
        )

    @Test
    fun `arrays having exactly max uncut length are not cut`() =
        assertThat(
            IntArray(32) { it }.getFormatted() is_ equal
                to_ "[${(0..31).joinToString(", ")}]"
        )

    @Test
    fun `arrays exceeding max uncut length by one are cut`() =
        assertThat(
            IntArray(33) { it }.getFormatted() is_ equal
                to_ "[${(0..15).joinToString(", ")}, ...(1 more)..., " +
                    "${(17..32).joinToString(", ")}]"
        )

    @Test
    fun `formatting a Path object`() {
        val sut = Path.of("a", "b", "c")
        assertThat(sut.getFormatted() is_ equal to_ sut.toString())
    }

    @Test
    fun `reformattedLines for empty string yields empty string`() =
        Given {
            ""
        } when_ {
            reformattedLines()
        } then {
            assertThat(it is_ equal to "")
        }

    @Test
    fun `reformattedLines for string consisting of only whitespaces yields empty string`() =
        Given {
            " \t"
        } when_ {
            reformattedLines()
        } then {
            assertThat(it is_ equal to "")
        }

    @Test
    fun `reformattedLines cuts whitespaces in the front and back`() =
        Given {
            " \tlalala\t "
        } when_ {
            reformattedLines()
        } then {
            assertThat(it is_ equal to "lalala")
        }

    @Test
    fun `reformattedLines joins line break in the middle of a sentence`() =
        Given {
            "lorem\nipsum"
        } when_ {
            reformattedLines()
        } then {
            assertThat(it is_ equal to "lorem ipsum")
        }

    @Test
    fun `reformattedLines removes indentation after joined line break`() =
        Given {
            "lorem\n   ipsum"
        } when_ {
            reformattedLines()
        } then {
            assertThat(it is_ equal to "lorem ipsum")
        }

    @Test
    fun `reformattedLines keeps line break before special characters`() =
        Given {
            "lorem\n=ipsum"
        } when_ {
            reformattedLines()
        } then {
            assertThat(it is_ equal to "lorem\n=ipsum")
        }

    @Test
    fun `reformattedLines keeps line break after special characters`() =
        Given {
            "lorem=\nipsum"
        } when_ {
            reformattedLines()
        } then {
            assertThat(it is_ equal to "lorem=\nipsum")
        }

    @Test
    fun `reformattedLines keeps single blank line`() =
        Given {
            "lorem\n\nipsum"
        } when_ {
            reformattedLines()
        } then {
            assertThat(it is_ equal to "lorem\n\nipsum")
        }
}
