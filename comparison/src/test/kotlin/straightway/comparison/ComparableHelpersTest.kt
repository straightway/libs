/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.comparison

import org.junit.jupiter.api.Test
import straightway.assertions.assertThat
import straightway.assertions.does
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.assertions.throw_
import straightway.assertions.to_
import straightway.error.Panic

class ComparableHelpersTest {

    private data class TestComparable(val i: Int) : Comparable<TestComparable> {
        override fun compareTo(other: TestComparable) = i.compareTo(other.i)
    }

    @Test
    fun `min returns smallest item`() =
        assertThat(
            min(TestComparable(1), TestComparable(2), TestComparable(3))
                is_ equal to_ TestComparable(1)
        )

    @Test
    fun `max returns greatest item`() =
        assertThat(
            max(TestComparable(1), TestComparable(2), TestComparable(3))
                is_ equal to_ TestComparable(3)
        )

    @Test
    fun `extreme without arguments panics`() =
        assertThat({ extreme<TestComparable> { _, _ -> true } } does throw_.type<Panic>())

    @Test
    fun `extreme returns the item which is ahead of all others`() =
        assertThat(
            extreme(TestComparable(1), TestComparable(2), TestComparable(3)) {
                a, _ ->
                a == TestComparable(2)
            } is_ equal to_ TestComparable(2)
        )

    @Test
    fun `extreme true returns first item`() =
        assertThat(
            extreme(TestComparable(1), TestComparable(2), TestComparable(3)) {
                _, _ ->
                true
            } is_ equal to_ TestComparable(1)
        )

    @Test
    fun `extreme false returns last item`() =
        assertThat(
            extreme(TestComparable(1), TestComparable(2), TestComparable(3)) {
                _, _ ->
                false
            } is_ equal to_ TestComparable(3)
        )
}
