# service 

A kotlin library allowing to load and instantiate services from java modules.

## Example

    val serviceGetter = ModuleServiceGetter(mapOf(Application::class to kernel.moduleLayer))
    val app = serviceGetter.getService<Application>()

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:service:<version>"
    }
