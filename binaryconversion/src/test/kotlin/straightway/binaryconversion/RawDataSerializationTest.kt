/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.binaryconversion

import org.junit.jupiter.api.Test
import straightway.assertions.assertThat
import straightway.assertions.does
import straightway.assertions.empty
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.assertions.not
import straightway.assertions.throw_
import straightway.assertions.to_
import straightway.bdd.Given
import straightway.expr.minus

class RawDataSerializationTest {

    @Test
    fun `serialization yields non-empty result`() =
        assertThat("Hello".serializeToByteArray() is_ not - empty)

    @Test
    fun `serialization forwards exception`() =
        assertThat({ Unserializable().serializeToByteArray() } does throw_.exception)

    @Test
    fun `deserialization retrieves serialized object`() =
        Given { "Hello".serializeToByteArray() } when_ {
            deserializeTo<String>()
        } then {
            assertThat(it is_ equal to_ "Hello")
        }

    @Test
    fun `deserialization of trash throws`() =
        assertThat({ byteArrayOf(0).deserializeTo<String>() } does throw_.exception)

    private class Unserializable
}
