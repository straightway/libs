/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.binaryconversion

import org.junit.jupiter.api.Test
import straightway.assertions.assertThat
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.assertions.to_
import straightway.bdd.Given
import java.io.Serializable

class BinaryConverterTest_SerializableConversion {

    @Test
    fun `Int as serializable is converted raw`() =
        Given<Serializable> {
            83
        } when_ {
            toByteArray()
        } then {
            assertThat(it is_ equal to_ 83.toByteArray())
        }

    @Test
    fun `Long as serializable is converted raw`() =
        Given<Serializable> {
            83L
        } when_ {
            toByteArray()
        } then {
            assertThat(it is_ equal to_ 83L.toByteArray())
        }

    @Test
    fun `String as serializable is converted using UTF-8 bytes`() =
        Given<Serializable> {
            "Hello"
        } when_ {
            toByteArray()
        } then {
            assertThat(it is_ equal to_ "Hello".toByteArray(Charsets.UTF_8))
        }

    @Test
    fun `other serializable object is converted using raw data serialization`() {
        Given<Serializable> {
            OtherSerializable(83)
        } when_ {
            toByteArray()
        } then {
            assertThat(it is_ equal to_ serializeToByteArray())
        }
    }

    private data class OtherSerializable(val value: Int) : Serializable {
        companion object { const val serialVersionUID = 1L }
    }
}
