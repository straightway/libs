# binaryconversion 

A kotlin library providing means to convert numbers and strings as well as serializable objects into byte arrays, and vice versa. This is either done using the raw bit representation for integer numbers, or by serialization for everything else. It also provides base62 conversion of byte arrays.

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:binaryconversion:<version>"
    }
