# expr

Support for partially bound functional expressions with a nice string representation.

## Example

    val add = FunExpr("+") { a: Int, b: Int -> a + b }.combinable
    val increment = add - Value(1)
    assertEquals(3, increment(2))
    assertEquals(5, (add - Value(3) - Value(2))())
    assertEquals("+-3-5", (add - Value(3) - Value(2)).toString())

For real life examples, see straightway.assertions.

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:expr:<version>"
    }
