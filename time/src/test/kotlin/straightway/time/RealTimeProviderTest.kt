/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.time

import org.junit.jupiter.api.Test
import straightway.assertions.assertThat
import straightway.assertions.is_
import straightway.assertions.less
import straightway.assertions.than
import straightway.bdd.Given
import java.time.Duration
import java.time.LocalDateTime

class RealTimeProviderTest {

    @Test
    fun `returned date time is about the current time`() =
        Given {
            RealTimeProvider()
        } when_ {
            now
        } then {
            assertThat(
                Duration.between(it, LocalDateTime.now())
                    is_ less than Duration.ofSeconds(1)
            )
        }
}
