# time 

A kotlin library providing the TimeProvider interface for abstracting time dependent code from the real time.

## Example

    class SomeTimeDependentCode(private val timeProvider: TimeProvider)
    {
        fun whatTimeIsIt(): String = timeProvider.now.toString()
    }

    val x = SomeTimeDependentCode(RealTimeProvider())
    println(x.whatTimeIsIt())

    @Test
    fun `correct time is returned`() {
        val dateTime = LocalDateTime.of(2000, 1, 1, 13, 14, 15)
        val timeProvider: TimeProvider = mock {
            on { now }.thenReturn(dateTime)
        }
        val sut = SomeTimeDependentCode(timeProvider)
        assertThat(sut.whatTimeIsIt() is_ equal to dateTime.toString())
    }

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:time:<version>"
    }
