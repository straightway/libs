# files

A kotlin library providing means to create file paths using the div operator, which enhances the readability of the path specification in the source code.

## Example

    val path = Path.root / "my" / "custom" / "path"    

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:files:<version>"
    }
