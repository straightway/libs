/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.files

import org.junit.jupiter.api.Test
import straightway.assertions.*
import straightway.bdd.Given
import straightway.bdd.isNull
import java.nio.file.Files
import java.nio.file.Path

class PathHelpersTest {
    @Test
    fun `div operator adds component to path`() =
        Given {
            Path.of("root", "dir")
        } when_ {
            this / "subdir" / "subsub"
        } then {
            assertThat(it is_ equal to Path.of("root", "dir", "subdir", "subsub"))
        }

    @Test
    fun `div operator with two dots returns parent path`() =
        Given {
            Path.of("root", "dir")
        } when_ {
            this / ".."
        } then {
            assertThat(it is_ equal to Path.of("root"))
        }

    @Test
    fun `create a relative path from scratch`() =
        assertThat(RelativePath / "dir" / "subdir" is_ equal to Path.of("dir", "subdir"))

    @Test
    fun `div operator with Parent returns parent path`() =
        Given {
            Path.of("root", "dir")
        } when_ {
            this / Parent
        } then {
            assertThat(it is_ equal to Path.of("root"))
        }

    @Test
    fun `ifExists for existing file does executes block and returns result`() =
        Given {
            object : AutoCloseable {
                val baseDir = Files.createTempDirectory("AppsModelTest")

                override fun close() {
                    baseDir.toFile().deleteRecursively()
                }
            }
        } when_ {
            baseDir ifExists { "Executed" }
        } then {
            assertThat(it is_ equal to "Executed")
        }

    @Test
    fun `ifExists for not existing file does not execute block and returns null`() =
        Given {
            Path.of("root", "dir")
        } when_ {
            ifExists { fail("File does not exist, block should not be called") }
        } then isNull

    @Test
    fun `doWithFile executes action on file`() =
        Given {
            Path.of("root", "dir")
        } when_ {
            this / "subdir" doWithFile { this / "subsub" }
        } then {
            assertThat(it is_ equal to toFile() / "subdir" / "subsub")
        }

    @Test
    fun `doWithPath executes action on path`() =
        Given {
            Path.of("root", "dir")
        } when_ {
            this / "subdir" doWithPath { this / "subsub" }
        } then {
            assertThat(it is_ equal to this / "subdir" / "subsub")
        }

    @Test
    fun `containedFiles yields all existing files in a subdir`() =
        Given {
            object : AutoCloseable {
                val baseDir = Files.createTempDirectory("AppsModelTest")

                override fun close() {
                    baseDir.toFile().deleteRecursively()
                }

                init {
                    baseDir / "child1" doWithFile { writeText("child1Text") }
                    baseDir / "child2" doWithFile { writeText("child2Text") }
                }
            }
        } when_ {
            baseDir.containedFiles
        } then {
            assertThat(it is_ equal to setOf(baseDir / "child1", baseDir / "child2"))
        }

    @Test
    fun `containedFiles for not existing file yields empty result`() =
        Given {
            RelativePath / "notExistingDir"
        } when_ {
            containedFiles
        } then {
            assertThat(it is_ empty)
        }
}
