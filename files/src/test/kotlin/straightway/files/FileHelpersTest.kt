/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.files

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import straightway.assertions.assertThat
import straightway.assertions.empty
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.bdd.Given
import straightway.bdd.isNull
import java.nio.file.Files
import java.nio.file.Path

class FileHelpersTest {

    @Test
    fun `div operator adds component to path`() =
        Given {
            Path.of("root", "dir").toFile()
        } when_ {
            this / "subdir" / "subsub"
        } then {
            assertThat(it is_ equal to Path.of("root", "dir", "subdir", "subsub").toFile())
        }

    @Test
    fun `div operator with two dots returns parent path`() =
        Given {
            Path.of("root", "dir").toFile()
        } when_ {
            this / ".."
        } then {
            assertThat(it is_ equal to Path.of("root").toFile())
        }

    @Test
    fun `create a relative path from scratch`() =
        assertThat(RelativeFile / "dir" / "subdir" is_ equal to Path.of("dir", "subdir").toFile())

    @Test
    fun `div operator with Parent returns parent path`() =
        Given {
            Path.of("root", "dir").toFile()
        } when_ {
            this / Parent
        } then {
            assertThat(it is_ equal to Path.of("root").toFile())
        }

    @Test
    fun `doWithFile executes action on file`() =
        Given {
            Path.of("root", "dir").toFile()
        } when_ {
            this / "subdir" doWithFile { this / "subsub" }
        } then {
            assertThat(it is_ equal to this / "subdir" / "subsub")
        }

    @Test
    fun `doWithPath executes action on path`() =
        Given {
            Path.of("root", "dir").toFile()
        } when_ {
            this / "subdir" doWithPath { this / "subsub" }
        } then {
            assertThat(it is_ equal to toPath() / "subdir" / "subsub")
        }

    @Test
    fun `containedFiles yields all existing files in a subdir`() =
        Given {
            object : AutoCloseable {
                val baseDir = Files
                    .createTempDirectory("AppsModelTest").toFile()

                override fun close() {
                    baseDir.deleteRecursively()
                }

                init {
                    baseDir / "child1" doWithFile { writeText("child1Text") }
                    baseDir / "child2" doWithFile { writeText("child2Text") }
                }
            }
        } when_ {
            baseDir.containedFiles
        } then {
            assertThat(it is_ equal to setOf(baseDir / "child1", baseDir / "child2"))
        }

    @Test
    fun `containedFiles for not existing file yields empty result`() =
        Given {
            RelativeFile / "notExistingDir"
        } when_ {
            containedFiles
        } then {
            assertThat(it is_ empty)
        }

    @Test
    fun `ifExists for not existing file returns null`() =
        Given {
            RelativeFile / "notExistingFile"
        } when_ {
            ifExists<Unit> { fail("Should not be called") }
        } then isNull

    @Test
    fun `ifExists for existing file executes action and returns result`() =
        Given {
            object : AutoCloseable {
                val dir = Files.createTempDirectory("FileHelpersTest_ifExists").toFile()
                override fun close() { dir.deleteRecursively() }
                init {
                    dir / "file" doWithFile { writeText("Lalala") }
                }
            }
        } when_ {
            dir / "file" ifExists { "Executed on $this" }
        } then {
            assertThat(it is_ equal to "Executed on ${dir / "file"}")
        }

    @Test
    fun `ifExists for existing directory executes action and returns result`() =
        Given {
            object : AutoCloseable {
                val dir = Files.createTempDirectory("FileHelpersTest_ifExists").toFile()
                override fun close() { dir.deleteRecursively() }
            }
        } when_ {
            dir ifExists { "Executed on $this" }
        } then {
            assertThat(it is_ equal to "Executed on $dir")
        }
}
