/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.files

import java.io.File
import java.nio.file.Files
import java.nio.file.Files.exists
import java.nio.file.Path

object RelativePath {
    operator fun div(newComponent: String): Path = Path.of(newComponent)
}

object Parent

operator fun Path.div(newComponent: String): Path =
    if (newComponent == "..") parent else resolve(newComponent)

operator fun Path.div(@Suppress("UNUSED_PARAMETER") newComponent: Parent): Path = parent

infix fun <T> Path.ifExists(action: Path.() -> T): T? = if (exists(this)) action() else null

infix fun <T> Path.doWithFile(block: File.() -> T): T = toFile().block()

infix fun <T> Path.doWithPath(block: Path.() -> T): T = block()

val Path.containedFiles: List<Path> get() =
    if (exists(this)) Files.list(this).toList() else listOf()
