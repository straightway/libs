/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
@file:Suppress("MatchingDeclarationName")
package straightway.files

import java.io.File
import java.nio.file.Path

object RelativeFile {
    operator fun div(newComponent: String): File = File(newComponent)
}

operator fun File.div(newComponent: String): File =
    if (newComponent == "..") parentFile else this.resolve(newComponent)

operator fun File.div(@Suppress("UNUSED_PARAMETER") newComponent: Parent): File = parentFile

infix fun <T> File.doWithFile(block: File.() -> T): T = block()

infix fun <T> File.ifExists(action: File.() -> T): T? = if (exists()) action() else null

infix fun <T> File.doWithPath(block: Path.() -> T): T = toPath().block()

val File.containedFiles: List<File> get() = (listFiles() ?: arrayOf()).toList()
