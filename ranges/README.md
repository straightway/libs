# ranges 

A kotlin library providing basic range operations (union, difference, intersection, containment check), as well as a class containing a set of disjoint ranges and a comparator for ranges (for storing them in an ordered list).

## Status

This software is in pre-release state. Every aspect of it may change without announcement or
notification or downward compatibility. As soon as version 1.0 is reached, all subsequent
changes for sub versions will be downward compatible. Breaking changes will then only occur
with a new major version with according deprecation marking.

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:ranges:<version>"
    }
