/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import straightway.binaryconversion.toByteArray
import straightway.error.Panic
import java.io.Serializable

/**
 * Compute hash codes for data arrays.
 */
interface Hasher {

    /**
     * The properties of the hasher.
     */
    val properties: Properties

    /**
     * Gets the hash for the given data.
     * @param data The data for which a hash value is computed.
     * @return The hash value of the data. It must contain as first byte the encoded hash algorithm.
     */
    fun getHash(data: ByteArray): ByteArray

    /**
     * A factory for hashers.
     */
    interface Factory {

        /**
         * Create a new hasher.
         */
        fun createHasher(): Hasher

        /**
         * The properties of the hashers created by this factory.
         */
        val properties: Properties
    }

    /**
     * The properties of a hasher.
     */
    interface Properties {

        /**
         * The hash algorithm name.
         */
        val hashAlgorithm: String

        /**
         * The number of bits of the hash value.
         */
        val hashBits: Int

        companion object {

            /**
             * Create a new set of hasher properties.
             * @param hashAlgorithm The hash algorithm name.
             * @param hashBits The number of bits of the hash value.
             */
            operator fun invoke(hashAlgorithm: String, hashBits: Int): Properties =
                Impl(hashAlgorithm, hashBits)

            // region Private

            private data class Impl(
                override val hashAlgorithm: String,
                override val hashBits: Int
            ) : Properties, Serializable {
                companion object {
                    const val serialVersionUID = 1L
                }
            }

            // endregion
        }
    }

    companion object {
        const val HEADER_BYTES = 1
    }
}

/**
 * Get the has value for a serializable object.
 * @param obj The object for which a hash value is computed.
 * @return The hash value for that object.
 */
fun Hasher.getHash(obj: Serializable) = getHash(obj.toByteArray())

/**
 * Get the total number of output bytes specified by the hasher properties. This number
 * of bytes is a combination of the header bytes and the has bytes (defined by the number of
 * hash bits).
 */
val Hasher.Properties.totalHashOutputBytes get() =
    (
        if (hashBits <= 0)
            throw Panic("hashBits must be positive (got: $hashBits)")
        else (hashBits - 1) / Byte.SIZE_BITS + 1
        ) + Hasher.HEADER_BYTES
