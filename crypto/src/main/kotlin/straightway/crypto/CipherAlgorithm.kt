/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import straightway.error.Panic

/**
 * An enumeration of all supported cipher algorithms.
 * @param encoded The code of the cipher algorithm as single byte.
 * @param isSymmetric A value indicating if the algorithm is a symmetric one.
 */
enum class CipherAlgorithm(val encoded: Byte, val isSymmetric: Boolean) {
    AES256(0, isSymmetric = true),
    RSA2048(1, isSymmetric = false)
}

/**
 * Gets a cypher algorithm encoded as a single byte.
 */
val ByteArray.cipherAlgorithm: CipherAlgorithm get() =
    with(firstOrNull()) {
        if (this == null) throw Panic("Cannot get cipher algorithm of empty data")
        CipherAlgorithm.values().singleOrNull { it.encoded == this }
            ?: throw Panic("Invalid cipher algorithm encoding: $this")
    }
