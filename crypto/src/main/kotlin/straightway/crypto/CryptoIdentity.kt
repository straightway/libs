/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import java.io.Serializable

/**
 * A crypto identity allows signing and verifying of digital signatures, as well as
 * encrypting and decrypting of data.
 */
interface CryptoIdentity : Signer, SignatureChecker, Cryptor {

    /**
     * The asymmetric key pair as binary byte array.
     */
    val rawKeyPair: ByteArray

    /**
     * A factory for crypto identites.
     */
    interface Factory : Signer.Factory, SignatureChecker.Factory {
        /**
         * Create a crypto identity with a random key.
         */
        fun createCryptoIdentity(): CryptoIdentity

        /**
         * Get the crypto identity for the given binary key.
         */
        fun getCryptoIdentity(rawKeyPair: ByteArray): CryptoIdentity

        /**
         * The properties of the created crypto identities.
         */
        override val properties: Cryptor.Properties
    }

    /**
     * A convenience base class for crypto identities, consisting of a signer and a signature
     * checker.
     * @param hashAlgorithm The hash algorithm used for signing and signature verification.
     * @param signer The signer part of the crypto identity.
     * @param signatureChecker The signature checker part of the crypto identity.
     * @param rawKeyPair The public/private key pair as binary byte array.
     */
    open class Base constructor(
        override val hashAlgorithm: String,
        val signer: Signer,
        val signatureChecker: SignatureChecker,
        override val rawKeyPair: ByteArray
    ) : CryptoIdentity,
        Signer by signer,
        SignatureChecker by signatureChecker,
        Serializable {

        override val properties: Cryptor.Properties
            get() = Cryptor.Properties(signatureChecker.properties, signer.properties)

        companion object {
            const val serialVersionUID = 1L
        }
    }
}
