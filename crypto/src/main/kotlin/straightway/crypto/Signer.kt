/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

/**
 * Base interface for types allowing to create cryptographic signatures and decrypt
 * data.
 */
interface Signer : Decryptor, HasherUser {

    /**
     * The key used for signing data.
     */
    val signKey: ByteArray

    /**
     * Sign the given data.
     * @param toSign The data to sign.
     * @return The cryptographic signature of the data.
     */
    fun sign(toSign: ByteArray): ByteArray

    /**
     * A factory for creating signers.
     */
    interface Factory {

        /**
         * The properties of the signers created by the factory.
         */
        val properties: Decryptor.Properties

        /**
         * Create a new signer.
         * @param rawKey The sign key as binary byte array.
         * @return The new signer.
         */
        fun createSigner(rawKey: ByteArray): Signer
    }
}
