/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import straightway.error.Panic
import java.io.Serializable

/**
 * A cryptographic key together with an algorithm allowing encrypting and decrypting
 * byte arrays.
 */
interface Cryptor : Encryptor, Decryptor {

    /**
     * A factory for creating cryptors.
     */
    interface Factory {

        /**
         * Create a new cryptor with a random key.
         */
        fun createCryptor(): Cryptor

        /**
         * Get the cryptor for the given binary key.
         */
        fun getCryptor(rawKey: ByteArray): Cryptor

        /**
         * The properties of the created cryptors.
         */
        val properties: Properties
    }

    /**
     * The properties of the cryptor.
     */
    override val properties: Properties

    /**
     * The properties of a cryptor.
     */
    interface Properties : Encryptor.Properties, Decryptor.Properties {
        companion object {

            /**
             * Create a new set of cryptor properties out of two separate encryptor and
             * decryptor properties.
             * @param encryptorProperties The encryptor properties
             * @param decryptorProperties The decryptor properties
             */
            operator fun <E : Encryptor.Properties, D : Decryptor.Properties> invoke(
                encryptorProperties: E,
                decryptorProperties: D
            ): Properties = Impl(encryptorProperties, decryptorProperties)

            /**
             * Create a new set of cryptor properties.
             * @param maxClearTextBytes The maximum number of bytes for clear text to be encrypted.
             * May be Int.MAX_VALUE for unconstrained size.
             * @param blockBytes The number of bytes per cipher block.
             * @param fixedCipherTextBytes In case the cipher text has always the same size,
             * this argument contains this size. Zero if the size is variable.
             * @param keyProperties The properties of the used keys.
             * @param outputSizeGetter A function returning the number of cipher text bytes for a
             * given number of clear text bytes.
             * @return The properties object.
             */
            operator fun invoke(
                maxClearTextBytes: Int,
                blockBytes: Int,
                fixedCipherTextBytes: Int,
                keyProperties: KeyProperties,
                outputSizeGetter: (Int) -> Int
            ) = Properties(
                Encryptor.Properties(
                    maxClearTextBytes,
                    blockBytes,
                    keyProperties,
                    outputSizeGetter
                ),
                Decryptor.Properties(fixedCipherTextBytes, keyProperties)
            )
        }

        // region Private

        private data class Impl<E : Encryptor.Properties, D : Decryptor.Properties>(
            private val encryptorProperties: E,
            private val decryptorProperties: D
        ) : Properties,
            Encryptor.Properties by encryptorProperties,
            Decryptor.Properties by decryptorProperties,
            Serializable {

            override val algorithm get() = encryptorProperties.algorithm
            override val keyBits get() = encryptorProperties.keyBits

            init {
                if (encryptorProperties.algorithm != decryptorProperties.algorithm ||
                    encryptorProperties.keyBits != decryptorProperties.keyBits
                )
                    throw Panic(
                        "Contradicting key properties: $encryptorProperties " +
                            "vs. $decryptorProperties"
                    )
            }

            companion object {
                const val serialVersionUID = 1L
            }
        }

        // endregion
    }
}
