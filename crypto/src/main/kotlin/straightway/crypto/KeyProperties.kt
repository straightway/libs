/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import java.io.Serializable

/**
 * Properties of a crypto key.
 */
interface KeyProperties {

    /**
     * The cipher algorithm the key is for.
     */
    val algorithm: String

    /**
     * The number of bits of the key.
     */
    val keyBits: Int

    companion object {

        /**
         * Create a new set of key properties
         * @param algorithm The cipher algorithm the key is for.
         * @param keyBits The number of bits of the key.
         */
        operator fun invoke(algorithm: String, keyBits: Int): KeyProperties =
            Impl(algorithm, keyBits)
    }

    // region Private

    private data class Impl(
        override val algorithm: String,
        override val keyBits: Int
    ) : KeyProperties, Serializable {
        companion object {
            const val serialVersionUID = 1L
        }
    }

    // endregion
}
