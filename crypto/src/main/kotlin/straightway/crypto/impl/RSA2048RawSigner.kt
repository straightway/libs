/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto.impl

import straightway.crypto.*
import straightway.crypto.impl.internal.privateKeyFrom
import straightway.crypto.impl.internal.stripAndCheckAlgorithmType
import straightway.crypto.impl.internal.with
import java.io.Serializable
import java.security.PrivateKey
import java.security.Signature
import javax.crypto.Cipher

/**
 * Signer implementation generating signatures and decrypting payload keys
 * using RSA algorithm with 2048 key bits, while the payload itself is symmetrically decrypted.
 * @param key The RSA 2048 private key.
 * @param hasherFactory A factory for creating the hashers used to create or verify digital
 * signatures.
 */
class RSA2048RawSigner(
    val key: PrivateKey,
    hasherFactory: Hasher.Factory
) : Signer, Serializable {

    /**
     * Constructor.
     * @param rawKey The RSA 2048 private key as binary ByteArray.
     * @param hasherFactory A factory for creating the hashers used to create or verify digital
     * signatures.
     */
    constructor(rawKey: ByteArray, hasherFactory: Hasher.Factory) :
        this(
            privateKeyFrom(
                rawKey.stripAndCheckAlgorithmType(CipherAlgorithm.RSA2048),
                RSA2048.properties.algorithm
            ),
            hasherFactory
        )

    override val properties get() = RSA2048.properties

    override val signKey get() = key.encoded!!.with(CipherAlgorithm.RSA2048)
    override val decryptionKey get() = key.encoded!!.with(CipherAlgorithm.RSA2048)

    override val hashAlgorithm: String = hasherFactory.properties.hashAlgorithm

    override fun sign(toSign: ByteArray) =
        with(rawSigner) { update(toSign); sign()!! }

    override fun decrypt(toDecrypt: ByteArray) =
        decryptCipher.doFinal(toDecrypt)!!

    /**
     * Implementation of a Signer factory creating RSA2048RawSigners.
     * @param hasherFactory A factory for creating the hashers used to create or verify
     * digital signatures.
     */
    class Factory(
        private val hasherFactory: Hasher.Factory
    ) : Signer.Factory, Serializable {

        override val properties get() = RSA2048.properties

        override fun createSigner(rawKey: ByteArray) =
            RSA2048RawSigner(rawKey, hasherFactory)

        companion object {
            const val serialVersionUID = 1L
        }
    }

    // region Private

    private val rawSigner get() =
        Signature.getInstance(signAlgorithm)!!.apply { initSign(key) }

    private val signAlgorithm =
        "${hashAlgorithm}with${properties.algorithm}"

    private val decryptCipher: Cipher get() =
        Cipher.getInstance(properties.algorithm)!!.apply {
            init(Cipher.DECRYPT_MODE, key)
        }

    // endregion

    companion object {
        const val serialVersionUID = 1L
    }
}
