/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import java.io.Serializable

/**
 * Encrypt data using a fixed internal key.
 */
interface Encryptor {

    /**
     * The properties of the encryptor.
     */
    val properties: Properties

    /**
     * The encryption key as binary byte array.
     */
    val encryptionKey: ByteArray

    /**
     * Encrypt data.
     * @param toEncrypt The data to encrypt.
     * @return The binary encrypted data.
     */
    fun encrypt(toEncrypt: ByteArray): ByteArray

    /**
     * The properties of an encryptor.
     */
    interface Properties : KeyProperties {

        /**
         * The maximum number of bytes for clear text to be encrypted.
         * May be Int.MAX_VALUE for unconstrained size.
         */
        val maxClearTextBytes: Int

        /**
         * The number of bytes per cipher block.
         */
        val blockBytes: Int

        /**
         * Returns the number of cipher text bytes for a given number of clear text bytes.
         * @param inputSize The number of bytes of the input data.
         * @return The number of bytes of the encrypted output.
         */
        fun getOutputBytes(inputSize: Int): Int

        companion object {

            /**
             * Create a new set of encryptor properties.
             * @param maxClearTextBytes The maximum number of bytes for clear text to be encrypted.
             * May be Int.MAX_VALUE for unconstrained size.
             * @param blockBytes The number of bytes per cipher block.
             * @param keyProperties The properties of the used keys.
             * @param outputSizeGetter A function returning the number of cipher text bytes for a
             * given number of clear text bytes.
             * @return The properties object.
             */
            operator fun invoke(
                maxClearTextBytes: Int,
                blockBytes: Int,
                keyProperties: KeyProperties,
                outputSizeGetter: (Int) -> Int
            ): Properties =
                PropertiesImpl(
                    maxClearTextBytes,
                    blockBytes,
                    keyProperties,
                    outputSizeGetter
                )

            // region private

            private data class PropertiesImpl(
                override val maxClearTextBytes: Int,
                override val blockBytes: Int,
                private val keyProperties: KeyProperties,
                private val outputSizeGetter: (Int) -> Int
            ) : Properties, KeyProperties by keyProperties, Serializable {

                override fun getOutputBytes(inputSize: Int) =
                    outputSizeGetter(inputSize)

                override fun equals(other: Any?) =
                    other is Properties &&
                        other.maxClearTextBytes == maxClearTextBytes &&
                        other.blockBytes == blockBytes &&
                        KeyProperties(other.algorithm, other.keyBits) ==
                            KeyProperties(algorithm, keyBits)

                override fun hashCode() =
                    maxClearTextBytes.hashCode() xor
                        blockBytes.hashCode() xor
                        KeyProperties(algorithm, keyBits).hashCode()

                companion object {
                    const val serialVersionUID = 1L
                }
            }

            // endregion
        }
    }
}
