/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.crypto

import org.junit.jupiter.api.Test
import straightway.assertions.*
import straightway.bdd.Given
import straightway.bdd.panics

class CryptorTest {

    private val test get() =
        Given {
            object {
                val encryptor = Encryptor.Properties(
                    maxClearTextBytes = 5,
                    blockBytes = 7,
                    KeyProperties(
                        algorithm = "Algo",
                        keyBits = 11
                    )
                ) { it * 2 }
                val decryptor = Decryptor.Properties(
                    fixedCipherTextBytes = 13,
                    KeyProperties(
                        algorithm = "Algo",
                        keyBits = 11
                    )
                )
            }
        }

    @Test
    fun `property construction with separate encryptor and decryptor property sets`() =
        test when_ {
            Cryptor.Properties(encryptor, decryptor)
        } then {
            assertThat(
                it.maxClearTextBytes is_ equal
                    to_ encryptor.maxClearTextBytes
            )
            assertThat(
                it.blockBytes is_ equal
                    to_ encryptor.blockBytes
            )
            assertThat(
                it.fixedCipherTextBytes is_ equal
                    to_ decryptor.fixedCipherTextBytes
            )
            assertThat(
                it.algorithm is_ equal
                    to_ decryptor.algorithm
            )
            assertThat(
                it.keyBits is_ equal
                    to_ decryptor.keyBits
            )
        }

    @Test
    fun `property construction with flat values`() =
        test when_ {
            Cryptor.Properties(
                maxClearTextBytes = encryptor.maxClearTextBytes,
                blockBytes = encryptor.blockBytes,
                fixedCipherTextBytes = decryptor.fixedCipherTextBytes,
                KeyProperties(
                    algorithm = decryptor.algorithm,
                    keyBits = decryptor.keyBits
                )
            ) { it * 2 }
        } then {
            val keyProperties = KeyProperties(it.algorithm, it.keyBits)
            assertThat(
                Encryptor.Properties(
                    it.maxClearTextBytes,
                    it.blockBytes,
                    keyProperties,
                    it::getOutputBytes
                ) is_ equal to_ encryptor
            )
            assertThat(
                Decryptor.Properties(
                    it.fixedCipherTextBytes,
                    keyProperties
                ) is_ equal to_ decryptor
            )
        }

    @Test
    fun `property construction with contradicting key algorithms throws`() =
        test when_ {
            Cryptor.Properties(
                encryptor,
                Decryptor.Properties(
                    decryptor.fixedCipherTextBytes,
                    KeyProperties("Other", encryptor.keyBits)
                )
            )
        } then panics

    @Test
    fun `property construction with contradicting key bits throws`() =
        test when_ {
            Cryptor.Properties(
                encryptor,
                Decryptor.Properties(
                    decryptor.fixedCipherTextBytes,
                    KeyProperties(
                        encryptor.algorithm,
                        encryptor.keyBits + 1
                    )
                )
            )
        } then panics
}
