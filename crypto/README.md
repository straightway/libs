# crypto

Provide convenience for symmetric and asymmetric cryptography and hashing.

This library uses the concept of a _Cryptor_, which is a combination of an encryption
algorithm with a key. It can be linked either to a symmetric or to an asymmetric algorithm, and it
can be used to encrypt and decrypt. In addition, a _CryptoIdentity_ may also be used to sign and
verify signatures. A _Hasher_ in turn can compute hash values for binary data.

A Cryptor, a Hasher and a CryptoIdentity can be created using a _CryptoFactory_, which defines
default implementations for these entities.

Example:

    val cryptoFactory = CryptoFactory() 
    val myIdentity = cryptoFactory.cryptoIdentity.createCryptoIdentity()

    val clearText = "Hello World".toByteArray()

    // Sign and verify signature
    val signature = myIdentity.sign(clearText)
    assertThat(myIdentity.isSignatureValid(clearText, signature) is_ true)

    // encrypt and decrypt
    val cipherText = myIdentity.encrypt(clearText)
    val decrypted = myIdentity.decrypt(cipherText)
    assertThat(decrypted is_ equal to clearText)

    // Get the proper decryptor for a given key:
    val key = myIdentity.decryptionKey
    val decryptor = cryptoFactory.getDecryptorFor(key)

## Status

This software is in pre-release state. Every aspect of it may change without announcement or
notification or downward compatibility. As soon as version 1.0 is reached, all subsequent
changes for sub versions will be downward compatible. Breaking changes will then only occur
with a new major version with according deprecation marking.

AS THIS CODE CURRENTLY HAS SECURITY ISSUES, IT IS NOT SUITABLE FOR PRODUCTION YET!

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:crypto:<version>"
    }
