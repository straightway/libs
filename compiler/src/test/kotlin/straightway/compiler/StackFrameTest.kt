/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.compiler

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import straightway.assertions.assertThat
import straightway.assertions.equal
import straightway.assertions.is_

@Suppress("RedundantSuspendModifier") // To test suspend functions
class StackFrameTest {

    @Test
    fun `currentStackFrame returns frame of enclosed function`() {
        assertThat(testFunction().methodName is_ equal to "testFunction")
    }

    @Test
    fun `currentStackFrame returns frame of enclosed suspend function`() = runBlocking {
        assertThat(suspendTestFunction().methodName is_ equal to "suspendTestFunction")
    }

    @Test
    fun `currentStackFrame returns frame of enclosed inlined suspend function`() = runBlocking {
        assertThat(inlineCalledSuspendTestFunction().methodName is_ equal to "suspendTestFunction")
    }

    @Test
    fun `currentStackFrame returns frame of enclosed suspend method`() = runBlocking {
        val sut = TestClass()
        assertThat(sut.x().methodName is_ equal to "x")
    }

    @Test
    fun `callingStackFrame returns frame of calling function`() {
        assertThat(callingTestFunction().methodName is_ equal to "callingTestFunction")
    }

    @Test
    fun `callingStackFrame returns frame of calling suspend function`() = runBlocking {
        assertThat(
            suspendCallingTestFunction().methodName
                is_ equal to "suspendCallingTestFunction"
        )
    }

    @Test
    fun `stackFrame returns properFrame`() = runBlocking {
        assertThat(
            callWithIndexedStackFrame(3, 4).methodName
                is_ equal to "callWithIndexedStackFrame"
        )
    }

    @Test
    fun `stackFrame returns properFrame for suspending function`() = runBlocking {
        assertThat(
            suspendingCallWithIndexedStackFrame(3, 4).methodName
                is_ equal to "suspendingCallWithIndexedStackFrame"
        )
    }

    // region Private

    fun testFunction() = currentStackFrame
    suspend fun suspendTestFunction() = currentStackFrame
    suspend inline fun inlineCalledSuspendTestFunction() = suspendTestFunction()

    fun calledTestFunction() = callingStackFrame
    fun callingTestFunction() = calledTestFunction()
    suspend fun suspendCalledTestFunction() = callingStackFrame
    suspend fun suspendCallingTestFunction() = suspendCalledTestFunction()

    fun callWithIndexedStackFrame(depth: Int, stackFrameIndex: Long) =
        recursiveTestFunction(depth, stackFrameIndex)
    fun recursiveTestFunction(depth: Int, stackFrameIndex: Long): StackWalker.StackFrame =
        if (depth <= 0)
            stackFrame(stackFrameIndex)
        else recursiveTestFunction(depth - 1, stackFrameIndex)

    suspend fun suspendingCallWithIndexedStackFrame(depth: Int, stackFrameIndex: Long) =
        suspendingRecursiveTestFunction(depth, stackFrameIndex)
    suspend fun suspendingRecursiveTestFunction(
        depth: Int,
        stackFrameIndex: Long
    ): StackWalker.StackFrame =
        if (depth <= 0)
            stackFrame(stackFrameIndex)
        else suspendingRecursiveTestFunction(depth - 1, stackFrameIndex)

    private class TestClass {
        suspend fun x() = currentStackFrame
    }

    // endregion
}
