/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.compiler

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import straightway.assertions.*

class DecorateTest {

    @Test
    fun `calls op`() {
        var wasCalled = false
        decorate { wasCalled = true }
        assertThat(wasCalled is_ true)
    }

    @Test
    fun `calls returns result`() =
        assertThat(decorate { 83 } is_ equal to_ 83)

    @Test
    fun `suspended calls op`() = runBlocking {
        var wasCalled = false
        decorateSuspended { wasCalled = true }
        assertThat(wasCalled is_ true)
    }

    @Test
    fun `suspended calls returns result`() = runBlocking {
        assertThat(decorateSuspended { 83 } is_ equal to_ 83)
    }
}
