/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
// The methods below must be inlined in order to deliver the proper stack frames.
@file:Suppress("NOTHING_TO_INLINE")

package straightway.compiler

/**
 * Returns the stack frame of the currently called function.
 */
inline val currentStackFrame get() = stackFrame(0)

/**
 * Returns the stack frame of the currently calling function.
 */
inline val callingStackFrame get() = stackFrame(1)

/**
 * Returns the stack frame at the given index.
 * @param index The stack frame index to return. Index 0 stands for the current method call.
 * @return The stack frame with the given index.
 */
inline fun stackFrame(index: Long) =
    StackWalker.getInstance().walk { frames ->
        frames.skip(index).findFirst().orElse(null)
    }
