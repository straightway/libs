# compiler 

A kotlin library providing means for fine tuning how the compiler and tools and plugins operate on the source code.

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:compiler:<version>"
    }
