/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.tracing

import org.junit.jupiter.api.Test
import org.mockito.kotlin.mock
import straightway.assertions.assertThat
import straightway.assertions.empty
import straightway.assertions.equal
import straightway.assertions.has
import straightway.assertions.is_
import straightway.assertions.of
import straightway.assertions.size
import straightway.assertions.to_
import straightway.bdd.Given
import straightway.bdd.panics
import straightway.bdd.throws
import straightway.error.Panic
import straightway.time.TimeProvider
import java.time.LocalDateTime
import kotlin.reflect.KCallable

class BufferTracerTest {

    private class Tester {
        val currentTime = LocalDateTime.of(2000, 1, 2, 3, 4, 5)!!
        val timeProvider = mock<TimeProvider> { on { now }.thenAnswer { currentTime } }
        val tracer = BufferTracer(timeProvider)
        var traced = tracer.traces
        fun callTestReturn(value: Int) = testReturn(value)
        fun testReturn(value: Int) = tracer(value) { value + 1 }
        fun callTestPanic() = testPanic()
        fun testPanic(): Int = tracer { throw Panic("Panic") }
        fun callTestTrace(level: TraceLevel, message: String) = testTrace(level, message)
        fun testTrace(level: TraceLevel, message: String) = tracer {
            traceMessage(level) { message }
        }
    }

    private val test = Given { Tester() }

    @Test
    fun `result is traced`() {
        test when_ {
            callTestReturn(83)
        } then {
            assertEventSequence(
                { assertCallingEvent(Tester::callTestReturn) },
                { assertEnterEvent(Tester::testReturn, 83) },
                { assertReturnEvent(Tester::testReturn, 84) }
            )
        }
    }

    @Test
    fun `multiple events are traced`() {
        test when_ {
            callTestReturn(83)
            callTestReturn(83)
        } then {
            assertEventSequence(
                { assertCallingEvent(Tester::callTestReturn) },
                { assertEnterEvent(Tester::testReturn, 83) },
                { assertReturnEvent(Tester::testReturn, 84) },
                { assertCallingEvent(Tester::callTestReturn) },
                { assertEnterEvent(Tester::testReturn, 83) },
                { assertReturnEvent(Tester::testReturn, 84) }
            )
        }
    }

    @Test
    fun `exception is traced`() {
        test when_ {
            callTestPanic()
        } then panics {
            assertEventSequence(
                { assertCallingEvent(Tester::callTestPanic) },
                { assertEnterEvent(Tester::testPanic) },
                { assertPanic() }
            )
        }
    }

    @Test
    fun `returning null in onTrace interceptor disables tracing enter and return events`() =
        test while_ {
            tracer.onTrace { null }
        } when_ {
            testReturn(83)
        } then {
            assertThat(traced is_ empty)
        }

    @Test
    fun `returning null in onTrace interceptor disables tracing exception events`() =
        test while_ {
            tracer.onTrace { null }
        } when_ {
            testPanic()
        } then throws.exception {
            assertThat(traced is_ empty)
        }

    @Test
    fun `returning null in onTrace interceptor disables tracing message events`() =
        test while_ {
            tracer.onTrace { null }
        } when_ {
            testTrace(TraceLevel.Info, "Hello")
        } then {
            assertThat(traced is_ empty)
        }

    @Test
    fun `clear removes all traces`() =
        test while_ {
            testReturn(83)
        } when_ {
            tracer.clear()
        } then {
            assertThat(traced is_ empty)
        }

    @Test
    fun `trace traces a message`() =
        test when_ {
            callTestTrace(TraceLevel.Debug, "Hello World")
        } then {
            lateinit var callTraceEntry: StackWalker.StackFrame
            assertEventSequence(
                {
                    callTraceEntry = stackFrame
                    assertCallingEvent(Tester::callTestTrace)
                },
                {
                    assertThat(
                        stackFrame.fileName
                            is_ equal to_ callTraceEntry.fileName
                    )
                    assertEnterEvent(Tester::testTrace)
                },
                {
                    assertThat(
                        stackFrame.fileName
                            is_ equal to_ callTraceEntry.fileName
                    )
                    assertThat(event is_ equal to_ TraceEvent.Message)
                    assertThat(level is_ equal to_ TraceLevel.Debug)
                    assertThat(value is_ equal to_ "Hello World")
                },
                { assertReturnEvent(Tester::testTrace) }
            )
        }

    @Test
    fun `trace entry has thread id`() =
        test when_ {
            callTestTrace(TraceLevel.Debug, "Hello")
        } then {
            val currentThreadId = Thread.currentThread().id
            traced.forEach {
                assertThat((it as TraceEntry).threadId is_ equal to_ currentThreadId)
            }
        }

    @Test
    fun `exception trace entry has thread id`() =
        test when_ {
            callTestPanic()
        } then throws.exception {
            val currentThreadId = Thread.currentThread().id
            traced.forEach { entry ->
                assertThat((entry as TraceEntry).threadId is_ equal to_ currentThreadId)
            }
        }

    @Test
    fun `call contains time stamp`() =
        test when_ {
            callTestReturn(83)
        } then {
            assert(traced.all { (it as TraceEntry).timeStamp == currentTime })
        }

    private companion object {

        fun Tester.assertEventSequence(vararg asserts: TraceEntry.() -> Unit) {
            assertThat(traced has size of asserts.size)
            asserts.forEachIndexed { index, assert ->
                (traced[index] as TraceEntry).assert()
            }
        }

        fun TraceEntry.assertPanic() {
            assertFunctionCall(Tester::testPanic)
            assertThat(level is_ equal to_ TraceLevel.Unknown)
            assertThat(event is_ equal to_ TraceEvent.Exception)
            assert(value is Panic) { "Unexpected exception type: $value" }
        }

        fun TraceEntry.assertCallingEvent(method: KCallable<*>) {
            assertFunctionCall(method)
            assertThat(level is_ equal to_ TraceLevel.Unknown)
            assertThat(event is_ equal to_ TraceEvent.Calling)
            assertThat(value is_ null)
        }

        fun TraceEntry.assertEnterEvent(method: KCallable<*>, vararg params: Any?) {
            assertFunctionCall(method)
            assertThat(level is_ equal to_ TraceLevel.Unknown)
            assertThat(event is_ equal to_ TraceEvent.Enter)
            assertThat(value is_ equal to_ params)
        }

        fun TraceEntry.assertReturnEvent(method: KCallable<*>, returnValue: Any = Unit) {
            assertFunctionCall(method)
            assertThat(level is_ equal to_ TraceLevel.Unknown)
            assertThat(event is_ equal to_ TraceEvent.Return)
            assertThat(value is_ equal to_ returnValue)
        }

        fun TraceEntry.assertFunctionCall(method: KCallable<*>) {
            assert(stackFrame.isTestFunCall(method)) {
                "Unexpected function call: $stackFrame"
            }
        }

        fun StackWalker.StackFrame.isTestFunCall(method: KCallable<*>) =
            className.startsWith(Tester::class.java.name) && methodName == method.name
    }
}
