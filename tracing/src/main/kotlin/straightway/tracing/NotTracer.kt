/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.tracing

/**
 * Tracer implementation which does nothing.
 */
class NotTracer : Tracer, TraceProvider {
    override val traces = listOf<TraceEntry>()
    override fun clear() {
        // this tracer does nothing by intention
    }

    override fun onTrace(action: (TraceEntry) -> Any?) {
        // this tracer does nothing by intention
    }

    override fun <TResult> traceCall(
        call: StackWalker.StackFrame,
        caller: StackWalker.StackFrame,
        params: Array<out Any?>,
        action: MessageTracer.() -> TResult
    ): TResult = noMessageTracer.action()

    private companion object {
        private val noMessageTracer = object : MessageTracer {
            override fun traceMessage(level: TraceLevel, message: () -> String) {
                // this tracer does nothing by intention
            }
        }
    }
}
