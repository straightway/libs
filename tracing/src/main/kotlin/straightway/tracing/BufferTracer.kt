/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.tracing

import straightway.aoplite.Aspect
import straightway.time.TimeProvider
import java.util.Collections.synchronizedList

/**
 * Tracer which stores the trace messages in a buffer list.
 */
// Notice: Some methods are inlined because the currentStackTrace property should return
// the stack trace of the _calling_ method. This is achieved by inlining.
@Suppress("OVERRIDE_BY_INLINE", "NOTHING_TO_INLINE")
class BufferTracer(private val timeProvider: TimeProvider) : Tracer, TraceProvider {

    private var onTraceAction: (TraceEntry) -> Any? = { it }
    private val _traces = synchronizedList(mutableListOf<Any>())

    private val traceAspect = Aspect<TraceContext> {
        onEnter {
            tryAdd(Trace(caller, TraceEvent.Calling, TraceLevel.Unknown))
            tryAdd(Trace(call, TraceEvent.Enter, TraceLevel.Unknown, params))
        }
        onReturn {
            tryAdd(Trace(call, TraceEvent.Return, TraceLevel.Unknown, it))
        }
        onException {
            tryAdd(Trace(call, TraceEvent.Exception, TraceLevel.Unknown, it.exception))
        }
    }

    override fun onTrace(action: (TraceEntry) -> Any?) { onTraceAction = action }

    override val traces: List<Any> get() = _traces

    override fun clear() { _traces.clear() }

    override fun <TResult> traceCall(
        call: StackWalker.StackFrame,
        caller: StackWalker.StackFrame,
        params: Array<out Any?>,
        action: MessageTracer.() -> TResult
    ): TResult = with(TraceContext(call, caller, params)) {
        (traceAspect(this)) {
            action()
        }
    }

    private inner class TraceContext(
        val call: StackWalker.StackFrame,
        val caller: StackWalker.StackFrame,
        val params: Array<out Any?>
    ) : MessageTracer {
        override fun traceMessage(level: TraceLevel, message: () -> String) {
            tryAdd(Trace(call, TraceEvent.Message, level, message()))
        }
    }

    private data class Trace(
        val call: StackWalker.StackFrame,
        val event: TraceEvent,
        val level: TraceLevel,
        val value: Any? = null
    )

    private fun tryAdd(trace: Trace) = with(trace) {
        tryAdd(
            TraceEntry(
                timeProvider.now,
                Thread.currentThread().id,
                call,
                event,
                level,
                value
            )
        )
    }

    private fun tryAdd(traceEntry: TraceEntry) {
        val transformedTraceEntry = onTraceAction(traceEntry)
        if (transformedTraceEntry != null) _traces.add(transformedTraceEntry)
    }
}
