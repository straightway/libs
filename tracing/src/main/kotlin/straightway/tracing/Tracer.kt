/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.tracing

import straightway.compiler.callingStackFrame
import straightway.compiler.currentStackFrame
import straightway.time.RealTimeProvider
import straightway.time.TimeProvider

/**
 * Allow tracing method calls and custom messages.
 */
interface Tracer {
    fun onTrace(action: (TraceEntry) -> Any?)
    fun <TResult> traceCall(
        call: StackWalker.StackFrame,
        caller: StackWalker.StackFrame,
        params: Array<out Any?>,
        action: MessageTracer.() -> TResult
    ): TResult

    companion object {
        operator fun invoke(timeProvider: TimeProvider, isTracingEnabled: () -> Boolean): Tracer =
            if (isTracingEnabled()) BufferTracer(timeProvider) else NotTracer()
        operator fun invoke(isTracingEnabled: () -> Boolean): Tracer =
            if (isTracingEnabled()) BufferTracer(RealTimeProvider()) else NotTracer()
    }
}

// Must be inline to get the call stack frame of the calling function
@Suppress("NOTHING_TO_INLINE")
inline operator fun <TResult> Tracer.invoke(
    vararg params: Any?,
    noinline action: MessageTracer.() -> TResult
): TResult = traceCall(currentStackFrame, callingStackFrame, params, action)
