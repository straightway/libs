/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.tracing

/**
 * Allow tracing custom messages.
 */
interface MessageTracer {

    /**
     * Write a custom message to a trace stream.
     * @param level The trace level for the message.
     * @param message A function returning the message. This is not a plain string for
     * performance reasons, because tracing may be disabled. In this case, this message function
     * is not evaluated.
     */
    fun traceMessage(level: TraceLevel, message: () -> String)
}
