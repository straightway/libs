/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.tracing

import straightway.binaryconversion.toByteArray
import straightway.stringformat.getFormatted
import straightway.stringformat.getIndented
import straightway.stringformat.toHex
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * A trace entry resulting from a trace call.
 */
data class TraceEntry(
    val timeStamp: LocalDateTime,
    val threadId: Long,
    val stackFrame: StackWalker.StackFrame,
    val event: TraceEvent,
    val level: TraceLevel,
    val value: Any?
) {
    override fun toString(): String {
        val timestamp =
            timeStamp.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"))
        val threadPrefix = "$timestamp [$threadIdHex] "
        val content = "$levelString$stackFrame ${event.description}$valueString"
        val indentation = threadPrefix.length
        val formattedContent =
            content.getIndented(indentation).removeRange(threadPrefix.indices)
        return "$threadPrefix$formattedContent"
    }

    private val threadIdHex get() =
        threadIdBytes.joinToString("") { it.toHex() }

    private val threadIdBytes get() =
        with(threadId.toByteArray().dropWhile { it == NULL }) {
            ifEmpty { listOf(NULL) }
        }

    private val valueString get() = if (value == null) "" else ": ${value.getFormatted()}"
    private val levelString get() = if (level == TraceLevel.Unknown) "" else "$level: "

    private companion object {
        const val NULL: Byte = 0
    }
}
