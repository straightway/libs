/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
plugins {
    kotlin("jvm") apply false
    kotlin("plugin.serialization") apply false
    id("org.javamodularity.moduleplugin") apply false
    id("org.jmailen.kotlinter") apply false
    id("io.gitlab.arturbosch.detekt") apply false
    id("de.undercouch.download") apply false
    id("org.jetbrains.dokka") apply false
    id("org.cadixdev.licenser") apply false
    id("org.sonarqube") apply false
    id("org.jetbrains.kotlinx.kover")
}

allprojects {
    repositories {
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/kotlinx-html/maven")
        maven("https://repo1.maven.org/maven2")
        maven("file:pages/repo")
        maven("https://straightway.codeberg.page/repo")
        mavenLocal()
    }
}

subprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "java")
    apply(plugin = "org.javamodularity.moduleplugin")
    apply(plugin = "org.jmailen.kotlinter")
    apply(plugin = "io.gitlab.arturbosch.detekt")
    apply(plugin = "maven-publish")
    apply(plugin = "kotlinx-serialization")
    apply(plugin = "org.cadixdev.licenser")
    apply(plugin = "org.sonarqube")

    extra.set("uploadToArchive", false)
    val repoUri = uri("file://" + rootProject.file("pages/repo"))
    val configFile = project.file("config.gradle.kts")
    if (configFile.exists())
        apply(from = configFile)

    dependencies {
        val junitVersion = "${project.properties["straightway.version.junit"]}"
        "implementation"(kotlin("stdlib"))
        "implementation"(kotlin("reflect"))
        "testImplementation"("${project.properties["straightway.lib.junit-jupiter-api"]}:$junitVersion")
        "testImplementation"("${project.properties["straightway.lib.junit-jupiter-params"]}:$junitVersion")
        "testImplementation"("${project.properties["straightway.lib.mockito-kotlin"]}")
        "testRuntimeOnly"("${project.properties["straightway.lib.junit-jupiter-engine"]}:$junitVersion")

        if (name != "bdd" && name != "assertions") {
            "testImplementation"(project(":expr"))
            "testImplementation"(project(":bdd"))
            "testImplementation"(project(":assertions"))
        }
    }

    configure<io.gitlab.arturbosch.detekt.extensions.DetektExtension> {
        allRules = true
        buildUponDefaultConfig = true
        config = files("$rootDir/config/detekt/config.yml")

        reports {
            html.required.set(true)
            xml.required.set(false)
            txt.required.set(false)
        }
    }

    configure<org.jmailen.gradle.kotlinter.KotlinterExtension> {
        disabledRules = arrayOf("no-wildcard-imports")
    }

    configure<JavaPluginExtension> {
        modularity.inferModulePath.set(true)
    }

    if (extra.get("uploadToArchive") == true) {
        val projectName = project.path.substring(1)
        configure<PublishingExtension> {
            publications {
                create<MavenPublication>(projectName) {
                    if (extra.has("mavenGroupId"))
                        groupId = extra.get("mavenGroupId").toString()
                    from(components["java"])
                }
            }

            repositories {
                maven {
                    name = "straightway"
                    url = repoUri
                }
            }
        }
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_16.toString()
        }
    }

    tasks.withType<JavaCompile> {
        options.release.set(16)
    }

    tasks.koverVerify {
        rule {
            name = "Minimal instruction coverage in percent"
            bound {
                minValue = 95
                valueType = kotlinx.kover.api.VerificationValueType.COVERED_LINES_PERCENTAGE
            }
        }
    }

    tasks.named<Test>("test") {
        finalizedBy(tasks.named<Task>("koverReport"))
        useJUnitPlatform()
        if (extensions.findByType(org.javamodularity.moduleplugin.extensions.TestModuleOptions::class) != null)
            extensions.configure(org.javamodularity.moduleplugin.extensions.TestModuleOptions::class) {
                runOnClasspath = true
            }
        systemProperty(
            "straightway.runLongRunningTests",
            System.getProperty("straightway.runLongRunningTests"))
    }

    tasks.named<Task>("check") {
        dependsOn(tasks.named<Task>("licenseCheck"))
    }

    tasks.register<Task>("fcheck") {
        group = "verification"
        description = "Checks the code and fixes simple formatting problems automatically"
        dependsOn(tasks.named<Task>("formatKotlin"))
        dependsOn(tasks.named<Task>("licenseFormat"))
        finalizedBy(tasks.named<Task>("check"))
    }

    configure<org.cadixdev.gradle.licenser.LicenseExtension> {
        val licenseFile = project.resources.text.fromFile("$rootDir/config/LicenseHeader.txt")
        header.set(licenseFile)
        newLine.set(false)
    }
}

tasks.register<Task>("format") {
    val formatTasks = subprojects.getTasks<Task>("formatKotlin")
    takeGroupAndDescriptionFrom(formatTasks)
    formatTasks.forEach { dependsOn(it) }
    val licenseFormatTasks = subprojects.getTasks<Task>("licenseFormat")
    licenseFormatTasks.forEach { dependsOn(it) }
}

tasks.register<Task>("test") {
    val testTasks = subprojects.getTasks<Task>("test")
    takeGroupAndDescriptionFrom(testTasks)
    testTasks.forEach { dependsOn(it) }
    finalizedBy(tasks.named<Task>("koverMergedReport"))
}

inline fun <reified T: Task> Iterable<Project>.getTasks(name: String): List<T> =
    mapNotNull {
        try {
            it.tasks.named<T>(name)
        } catch (x: UnknownTaskException) {
            null
        }
    }.map {
        it.get()
    }

fun Task.takeGroupAndDescriptionFrom(others: List<Task>) =
    others.firstOrNull()?.let {
        group = it.group ?: ""
        description = it.description ?: ""
    }