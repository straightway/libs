/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.aoplite

import org.junit.jupiter.api.Test
import straightway.assertions.assertThat
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.bdd.Given

class MethodCallContextTest {

    @Test
    fun `passes calling method stack frame as call`() {
        var frame: StackWalker.StackFrame? = null
        Given {
            Aspect<MethodCallContext> {
                onEnter { frame = call }
            }
        } when_ {
            notSuspendingFun(this)
        } then {
            assertThat(frame?.methodName is_ equal to "notSuspendingFun")
        }
    }

    @Test
    fun `passes suspending calling method stack frame as call`() {
        var frame: StackWalker.StackFrame? = null
        Given {
            Aspect<MethodCallContext> {
                onEnter { frame = call }
            }
        } whenBlocking {
            suspendingFun(this)
        } then {
            assertThat(frame?.methodName is_ equal to "suspendingFun")
        }
    }

    // region Private

    private fun notSuspendingFun(sut: Aspect<MethodCallContext>) = sut { 83 }

    private suspend fun suspendingFun(sut: Aspect<MethodCallContext>) = sut.suspending { 83 }

    // endregion
}
