/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.aoplite

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import straightway.assertions.assertThat
import straightway.assertions.equal
import straightway.assertions.is_
import straightway.assertions.to_
import straightway.bdd.Given
import straightway.bdd.panics
import straightway.bdd.throws
import straightway.error.Panic

@Suppress("IMPLICIT_NOTHING_TYPE_ARGUMENT_IN_RETURN_POSITION")
class AspectTest {

    @Test
    fun `invoke executes given function`() =
        Given {
            Aspect<String> {}
        } when_ {
            (this("Hello")) { 83 }
        } then {
            assertThat(it is_ equal to_ 83)
        }

    @Test
    fun `onEnter is executed before the action`() {
        var isBeforeActionCalled = false
        var isActionCalled = false
        var tag = ""
        Given {
            Aspect<String> {
                onEnter {
                    isBeforeActionCalled = true
                    tag = this
                }
            }
        } when_ {
            (this("Hello")) {
                assertThat(isBeforeActionCalled is_ true)
                isActionCalled = true
            }
        } then {
            assertThat(isActionCalled is_ true)
            assertThat(tag is_ equal to "Hello")
        }
    }

    @Test
    fun `excpetion in onEnter action aborts execution and throws the exception`() {
        var isActionCalled = false
        Given {
            Aspect<String> {
                onEnter { throw Panic("Panic") }
            }
        } when_ {
            (this("Hello")) {
                isActionCalled = true
            }
        } then panics {
            assertThat(isActionCalled is_ false)
        }
    }

    @Test
    fun `onLeave is executed after the action in normal execution`() {
        var isAfterActionCalled = false
        var isActionCalled = false
        var tag = ""
        Given {
            Aspect<String> {
                onLeave {
                    isAfterActionCalled = true
                    tag = this
                }
            }
        } when_ {
            (this("Hello")) {
                assertThat(isAfterActionCalled is_ false)
                isActionCalled = true
            }
        } then {
            assertThat(isActionCalled is_ true)
            assertThat(isAfterActionCalled is_ true)
            assertThat(tag is_ equal to "Hello")
        }
    }

    @Test
    fun `onLeave is executed after the action on exception`() {
        var isAfterActionCalled = false
        Given {
            Aspect<String> {
                onLeave { isAfterActionCalled = true }
            }
        } when_ {
            (invoke("Hello")).nothing<Int> { throw Panic("Panic") }
        } then panics {
            assertThat(isAfterActionCalled is_ true)
        }
    }

    @Test
    fun `onLeave is executed after the action on exception in before`() {
        var isAfterActionCalled = false
        Given {
            Aspect<String> {
                onEnter { throw Panic("Panic") }
                onLeave { isAfterActionCalled = true }
            }
        } when_ {
            (this("Hello")) { }
        } then throws.exception {
            assertThat(isAfterActionCalled is_ true)
        }
    }

    @Test
    fun `exception in onLeave overrides exception in before`() {
        Given {
            Aspect<String> {
                onEnter { throw IllegalAccessException() }
                onLeave { throw Panic("Panic in onLeave") }
            }
        } when_ {
            (this("Hello")) { }
        } then panics
    }

    @Test
    fun `exception in onLeave overrides Exception in action`() {
        Given {
            Aspect<String> {
                onLeave { throw Panic("Panic in onLeave") }
            }
        } when_ {
            (this("Hello")).nothing<Int> { throw IllegalAccessException() }
        } then panics
    }

    @Test
    fun `onException is executed on exception in action`() {
        var isOnExceptionCalled = false
        var tag = ""
        Given {
            Aspect<String> {
                onException {
                    isOnExceptionCalled = true
                    tag = this
                }
            }
        } when_ {
            (invoke("Hello")).nothing<Int> { throw Panic("Aaaah!!!") }
        } then panics {
            assertThat(it.state is_ equal to "Aaaah!!!")
            assertThat(isOnExceptionCalled is_ true)
            assertThat(tag is_ equal to "Hello")
        }
    }

    @Test
    fun `onException may ignore exception and return a value`() {
        var valuePassedInReturnCallback: Any? = null
        Given {
            Aspect<String> {
                onException {
                    assertThat(it.isReturningValue is_ false)
                    it.returnWith(83)
                    assertThat(it.isReturningValue is_ true)
                }
                onReturn {
                    valuePassedInReturnCallback = it
                }
            }
        } when_ {
            (invoke("Hello")) {
                throw Panic("Aaaah!!!")

                // Determine return type
                @Suppress("UNREACHABLE_CODE") 23
            }
        } then {
            assertThat(it is_ equal to 83)
            assertThat(valuePassedInReturnCallback is_ equal to 83)
        }
    }

    @Test
    fun `exception in onLeave overrides exception in onException`() {
        Given {
            Aspect<String> {
                onException { throw IllegalAccessException() }
                onLeave { throw Panic("Panic") }
            }
        } when_ {
            invoke("Hello").nothing<Int> { throw IllegalAccessException() }
        } then panics
    }

    @Test
    fun `onReturn is not called after exception in onEnter`() {
        Given {
            Aspect<String> {
                onEnter { throw Panic("Panic") }
                onReturn { fail("onReturn called") }
            }
        } when_ {
            (this("Hello")) { 83 }
        } then panics
    }

    @Test
    fun `onReturn is not called after exception in invoke`() {
        Given {
            Aspect<String> {
                onReturn { fail("onReturn called") }
            }
        } when_ {
            invoke("Hello").nothing<Int> { throw Panic("Panic") }
        } then panics
    }

    @Test
    fun `onReturn is called on successful return`() {
        var isOnReturnCalled = false
        var context = ""
        Given {
            Aspect<String> {
                onReturn {
                    isOnReturnCalled = true
                    context = this
                }
            }
        } when_ {
            (this("Hello")) { 83 }
        } then {
            assertThat(it is_ equal to_ 83)
            assertThat(isOnReturnCalled is_ true)
            assertThat(context is_ equal to "Hello")
        }
    }

    @Test
    fun `onExecuteAction is called`() {
        var isOnExecuteActionCalled = false
        Given {
            Aspect<String> {
                onExecute {
                    isOnExecuteActionCalled = true
                    it()
                }
            }
        } when_ {
            (this("Hello")) { 83 }
        } then {
            assertThat(it is_ equal to_ 83)
            assertThat(isOnExecuteActionCalled is_ true)
        }
    }

    @Test
    fun `onExecuteAction may alter result`() =
        Given {
            Aspect<String> {
                onExecute { 23 }
            }
        } when_ {
            (this("Hello")) { 83 }
        } then {
            assertThat(it is_ equal to_ 23)
        }

    @Test
    fun `onExecuteAction with wrong result type causes exception`() =
        Given {
            Aspect<String> {
                onExecute { "Hi" }
            }
        } when_ {
            (this("Hello")) { 83 }
        } then throws.exception

    @Test
    fun `onExecuteAction throwing exception is passed on`() =
        Given {
            Aspect<String> {
                onExecute { throw Panic("Aaah!!") }
            }
        } when_ {
            (this("Hello")) { 83 }
        } then panics {
            assertThat(it.state is_ equal to "Aaah!!")
        }

    @Test
    fun `suspend function is intercepted`() {
        var isOnReturnCalled = false
        Given {
            Aspect<String> {
                onReturn {
                    isOnReturnCalled = true
                }
            }
        } whenBlocking {
            this("Hello").suspending { 83 }
        } then {
            assertThat(it is_ equal to_ 83)
            assertThat(isOnReturnCalled is_ true)
        }
    }

    @Test
    fun `plus calls onEnter for each sub aspect`() {
        val onEnterCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onEnter { onEnterCalls += "A$this" } }
            val b = Aspect<Int> { onEnter { onEnterCalls += "B$this" } }
            a("la") + b(83)
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onEnterCalls is_ equal to listOf("Ala", "B83"))
        }
    }

    @Test
    fun `plus calls onLeave for each sub aspect`() {
        val onLeaveCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onLeave { onLeaveCalls += "A$this" } }
            val b = Aspect<Int> { onLeave { onLeaveCalls += "B$this" } }
            a("la") + b(83)
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onLeaveCalls is_ equal to listOf("Ala", "B83"))
        }
    }

    @Test
    fun `plus calls onExecute for each sub aspect`() {
        val onExecuteCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onExecute { onExecuteCalls += "A$this=${it()}"; 2 } }
            val b = Aspect<Int> { onExecute { onExecuteCalls += "B$this=${it()}"; 3 } }
            a("la") + b(83)
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 3)
            assertThat(onExecuteCalls is_ equal to listOf("Ala=23", "B83=2"))
        }
    }

    @Test
    fun `plus calls onReturn for each sub aspect`() {
        val onReturnCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onReturn { onReturnCalls += "A$this $it" } }
            val b = Aspect<Int> { onReturn { onReturnCalls += "B$this $it" } }
            a("la") + b(83)
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onReturnCalls is_ equal to listOf("Ala 23", "B83 23"))
        }
    }

    @Test
    fun `plus calls onException for each sub aspect`() {
        val onExceptionCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onException { onExceptionCalls += "A$this $it" } }
            val b = Aspect<Int> { onException { onExceptionCalls += "B$this $it" } }
            a("la") + b(83)
        } when_ {
            nothing<Int> { throw Panic("Aaaah!!") }
        } then panics {
            assertThat(it.state is_ equal to "Aaaah!!")
            assertThat(
                onExceptionCalls is_ equal to listOf(
                    "Ala Panic: Aaaah!!",
                    "B83 Panic: Aaaah!!"
                )
            )
        }
    }

    @Test
    fun `times calls onEnter for each sub aspect`() {
        val onEnterCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onEnter { onEnterCalls += "A$this" } }
            val b = Aspect<String> { onEnter { onEnterCalls += "B$this" } }
            (a * b)("la")
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onEnterCalls is_ equal to listOf("Ala", "Bla"))
        }
    }

    @Test
    fun `times calls onLeave for each sub aspect`() {
        val onLeaveCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onLeave { onLeaveCalls += "A$this" } }
            val b = Aspect<String> { onLeave { onLeaveCalls += "B$this" } }
            (a * b)("la")
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onLeaveCalls is_ equal to listOf("Ala", "Bla"))
        }
    }

    @Test
    fun `times calls onExecute for each sub aspect`() {
        val onExecuteCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onExecute { onExecuteCalls += "A$this=${it()}"; 2 } }
            val b = Aspect<String> { onExecute { onExecuteCalls += "B$this=${it()}"; 3 } }
            (a * b)("83")
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 3)
            assertThat(onExecuteCalls is_ equal to listOf("A83=23", "B83=2"))
        }
    }

    @Test
    fun `times calls onReturn for each sub aspect`() {
        val onReturnCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onReturn { onReturnCalls += "A$this $it" } }
            val b = Aspect<String> { onReturn { onReturnCalls += "B$this $it" } }
            (a * b)("la")
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onReturnCalls is_ equal to listOf("Ala 23", "Bla 23"))
        }
    }

    @Test
    fun `times calls onException for each sub aspect`() {
        val onExceptionCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onException { onExceptionCalls += "A$this $it" } }
            val b = Aspect<String> { onException { onExceptionCalls += "B$this $it" } }
            (a * b)("la")
        } when_ {
            nothing<Int> { throw Panic("Aaaah!!") }
        } then panics {
            assertThat(it.state is_ equal to "Aaaah!!")
            assertThat(
                onExceptionCalls is_ equal to listOf(
                    "Ala Panic: Aaaah!!",
                    "Bla Panic: Aaaah!!"
                )
            )
        }
    }

    @Test
    fun `times on aspect with context left calls onEnter for each sub aspect`() {
        val onEnterCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onEnter { onEnterCalls += "A$this" } }
            val b = Aspect<String> { onEnter { onEnterCalls += "B$this" } }
            a("la") * b
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onEnterCalls is_ equal to listOf("Ala", "Bla"))
        }
    }

    @Test
    fun `times on aspect with context left calls onLeave for each sub aspect`() {
        val onLeaveCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onLeave { onLeaveCalls += "A$this" } }
            val b = Aspect<String> { onLeave { onLeaveCalls += "B$this" } }
            a("la") * b
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onLeaveCalls is_ equal to listOf("Ala", "Bla"))
        }
    }

    @Test
    fun `times on aspect with context left calls onExecute for each sub aspect`() {
        val onExecuteCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onExecute { onExecuteCalls += "A$this=${it()}"; 2 } }
            val b = Aspect<String> { onExecute { onExecuteCalls += "B$this=${it()}"; 3 } }
            a("83") * b
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 3)
            assertThat(onExecuteCalls is_ equal to listOf("A83=23", "B83=2"))
        }
    }

    @Test
    fun `times on aspect with context left calls onReturn for each sub aspect`() {
        val onReturnCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onReturn { onReturnCalls += "A$this $it" } }
            val b = Aspect<String> { onReturn { onReturnCalls += "B$this $it" } }
            a("la") * b
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onReturnCalls is_ equal to listOf("Ala 23", "Bla 23"))
        }
    }

    @Test
    fun `times on aspect with context left calls onException for each sub aspect`() {
        val onExceptionCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onException { onExceptionCalls += "A$this $it" } }
            val b = Aspect<String> { onException { onExceptionCalls += "B$this $it" } }
            a("la") * b
        } when_ {
            nothing<Int> { throw Panic("Aaaah!!") }
        } then panics {
            assertThat(it.state is_ equal to "Aaaah!!")
            assertThat(
                onExceptionCalls is_ equal to listOf(
                    "Ala Panic: Aaaah!!",
                    "Bla Panic: Aaaah!!"
                )
            )
        }
    }
    @Test
    fun `times on aspect with context right calls onEnter for each sub aspect`() {
        val onEnterCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onEnter { onEnterCalls += "A$this" } }
            val b = Aspect<String> { onEnter { onEnterCalls += "B$this" } }
            a * b("la")
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onEnterCalls is_ equal to listOf("Ala", "Bla"))
        }
    }

    @Test
    fun `times on aspect with context right calls onLeave for each sub aspect`() {
        val onLeaveCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onLeave { onLeaveCalls += "A$this" } }
            val b = Aspect<String> { onLeave { onLeaveCalls += "B$this" } }
            a * b("la")
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onLeaveCalls is_ equal to listOf("Ala", "Bla"))
        }
    }

    @Test
    fun `times on aspect with context right calls onExecute for each sub aspect`() {
        val onExecuteCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onExecute { onExecuteCalls += "A$this=${it()}"; 2 } }
            val b = Aspect<String> { onExecute { onExecuteCalls += "B$this=${it()}"; 3 } }
            a * b("83")
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 3)
            assertThat(onExecuteCalls is_ equal to listOf("A83=23", "B83=2"))
        }
    }

    @Test
    fun `times on aspect with context right calls onReturn for each sub aspect`() {
        val onReturnCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onReturn { onReturnCalls += "A$this $it" } }
            val b = Aspect<String> { onReturn { onReturnCalls += "B$this $it" } }
            a * b("la")
        } when_ {
            this { 23 }
        } then {
            assertThat(it is_ equal to 23)
            assertThat(onReturnCalls is_ equal to listOf("Ala 23", "Bla 23"))
        }
    }

    @Test
    fun `times on aspect with context right calls onException for each sub aspect`() {
        val onExceptionCalls = mutableListOf<String>()
        Given {
            val a = Aspect<String> { onException { onExceptionCalls += "A$this $it" } }
            val b = Aspect<String> { onException { onExceptionCalls += "B$this $it" } }
            a * b("la")
        } when_ {
            nothing<Int> { throw Panic("Aaaah!!") }
        } then panics {
            assertThat(it.state is_ equal to "Aaaah!!")
            assertThat(
                onExceptionCalls is_ equal to listOf(
                    "Ala Panic: Aaaah!!",
                    "Bla Panic: Aaaah!!"
                )
            )
        }
    }
}
