/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.aoplite

import kotlinx.coroutines.*
import org.junit.jupiter.api.Test
import straightway.assertions.*
import straightway.error.Panic

class Examples {

    private val context get() = object {
        val logOutput = mutableListOf<String>()
        val logOutputWithoutLines get() =
            logOutput.map { Regex("\\([^)]*\\)").replace(it, "") }
        val log = MethodCallAspect {
            onEnter { logOutput += "entering $call" }
            onReturn { logOutput += "returning from $call: $it" }
            onException {
                logOutput += "exception in $call: $it"
            }
        }
        val failsafe = MethodCallAspect {
            onException {
                it.returnWith(null)
            }
        }
        suspend fun timeout(timeoutMillis: Long) = MethodCallAspect {
            onExecute {
                withTimeout(timeoutMillis) { it() }
            }
        }
    }

    @Test
    fun `simple logger aspect`() = with(context) {
        val sut = object {
            fun func() = log {
                logOutput += "inside"
            }
        }
        sut.func()
        assertThat(
            logOutputWithoutLines is_ equal to listOf(
                "entering straightway.aoplite.Examples\$simple logger aspect\$1\$sut\$1.func",
                "inside",
                "returning from straightway.aoplite.Examples\$simple logger aspect\$1\$sut\$1." +
                    "func: kotlin.Unit"
            )
        )
    }

    @Test
    fun `failsafe aspect`() = with(context) {
        val sut = object {
            fun func(): String? = failsafe.invoke {
                throw Panic("Aaaaah!")
            }
        }
        assertThat(sut.func() is_ null)
    }

    @Test
    fun `timeout aspect`() = runBlocking {
        with(context) {
            runBlocking {
                val sut = object {
                    suspend fun func(infinite: Boolean) = timeout(100).suspending {
                        while (infinite) { yield() }
                        23
                    }
                }

                assertThat(sut.func(false) is_ equal to 23)
                assertThat(
                    suspend { sut.func(true) }
                        does throw_.type<TimeoutCancellationException>()
                )
            }
        }
    }

    @Test
    fun `combined aspects`() = runBlocking {
        with(context) {
            runBlocking {
                val sut = object {
                    suspend fun func(infinite: Boolean): Int? =
                        (log * failsafe * timeout(100)).suspending<Int?> {
                            while (infinite)
                                yield()
                            23
                        }
                }

                assertThat(sut.func(false) is_ equal to 23)
                assertThat(sut.func(true) is_ null)
                assertThat(
                    logOutputWithoutLines is_ equal to listOf(
                        "entering straightway.aoplite.Examples\$" +
                            "combined aspects\$1\$1\$1\$sut\$1.func",
                        "returning from straightway.aoplite.Examples\$" +
                            "combined aspects\$1\$1\$1\$sut\$1.func: 23",
                        "entering straightway.aoplite.Examples\$" +
                            "combined aspects\$1\$1\$1\$sut\$1.func",
                        "exception in straightway.aoplite.Examples\$" +
                            "combined aspects\$1\$1\$1\$sut\$1.func: " +
                            "kotlinx.coroutines.TimeoutCancellationException: " +
                            "Timed out waiting for 100 ms",
                        "returning from straightway.aoplite.Examples\$" +
                            "combined aspects\$1\$1\$1\$sut\$1.func: null",
                    )
                )
            }
        }
    }
}
