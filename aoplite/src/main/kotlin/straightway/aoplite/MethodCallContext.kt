/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.aoplite

import straightway.compiler.currentStackFrame

/**
 * Shorthand type for an interceptor with a method call context.
 */
typealias MethodCallAspect = Aspect<MethodCallContext>

/**
 * An interceptor context which contains a stack frame.
 * @param call The stack frame of the call which is subject to the method call context.
 */
data class MethodCallContext(val call: StackWalker.StackFrame) {
    companion object {
        /**
         * Creates an interceptor context with the current stack frame.
         */
        inline val current get() =
            MethodCallContext(currentStackFrame)
    }
}

/**
 * Execute code with a MethodCallContext interceptor, passing the current stack frame
 * with the method call context.
 */
@Suppress("NOTHING_TO_INLINE") // Must be inline to get context of calling method
inline operator fun <reified TResult> MethodCallAspect.invoke(
    noinline action: () -> TResult
) = methodCallAspectWithContext.invoke(action)

/**
 * Execute suspending code with a MethodCallContext interceptor, passing the current stack frame
 * with the method call context.
 */
suspend inline fun <reified TResult> MethodCallAspect.suspending(
    noinline action: suspend () -> TResult
) = methodCallAspectWithContext.suspending(action)

// region Private

inline val MethodCallAspect.methodCallAspectWithContext: Aspect.WithContext<MethodCallContext>
    get() = Aspect.WithContext(MethodCallContext.current, this)

// endregion
