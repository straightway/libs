/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.aoplite

import kotlinx.coroutines.runBlocking

/**
 * Execute custom actions before and after the code passed to the invoke method. Also execute custom
 * actions when that code throws an exception.
 * @param init An initializer code block which can be used to register callbacks for interception
 * points.
 * @param TContext The type of the context passed with each callback invocation. It may e.g.
 * contain a StackWalker.StackFrame to identify the method.
 */
class Aspect<TContext>(
    init: Initializer<TContext>.() -> Unit
) {

    // region Private fields

    private var onEnterAction: suspend TContext.() -> Unit = {}
    private var onLeaveAction: suspend TContext.() -> Unit = {}
    private var onExecuteAction: suspend TContext.(toExecute: suspend () -> Any?) -> Any? = { it() }
    private var onReturnAction: suspend TContext.(result: Any?) -> Unit = {}
    private var onExceptionAction: suspend TContext.(ExceptionContext) -> Unit = {}

    // endregion

    /**
     * Initialization interface for interceptors.
     */
    interface Initializer<TContext> {

        /**
         * Register a callback that is called when the Interceptor's invoke method is called.
         */
        fun onEnter(action: suspend TContext.() -> Unit)

        /**
         * Register a callback that is called when the Interceptor's invoke method is left,
         * i.e. either by return or exception.
         */
        fun onLeave(action: suspend TContext.() -> Unit)

        /**
         * Register a callback that is called to call the Aspect's invoke method.
         * @param action The action to call. Must return a value of proper type.
         */
        fun onExecute(action: suspend TContext.(toExecute: suspend () -> Any?) -> Any?)

        /**
         * Register a callback that is called when the Interceptor's invoke method is left
         * by return.
         */
        fun onReturn(action: suspend TContext.(result: Any?) -> Unit)

        /**
         * Register a callback that is called when the Interceptor's invoke method is left
         * by exception.
         */
        fun onException(action: suspend TContext.(ExceptionContext) -> Unit)
    }

    /**
     * An exception context is passed as argument to the onException callback.
     */
    interface ExceptionContext {

        /**
         * The thrown exception.
         */
        val exception: Throwable

        /**
         * A value indicating whether the exception should be ignored and a value
         * returned.
         */
        var isReturningValue: Boolean

        /**
         * Define that the exception should be ignored and the given value should be
         * returned. It must be compatible to the return type of the intercepted function.
         */
        fun returnWith(returnValue: Any?)
    }

    /**
     * Attaches the given context to the aspect, so that it can be invoked or combined
     * with other contextualized aspects (using the + operator).
     * @param context The context to attach to the aspect.
     */
    operator fun invoke(context: TContext) = WithContext(context, this)

    /**
     * Combine two aspects with the same context.
     * @param other The other aspect to combine with this one.
     * @return A new aspect considering both input aspects.
     */
    operator fun times(other: Aspect<TContext>): Aspect<TContext> =
        Aspect {
            onEnter {
                onEnterAction()
                (other.onEnterAction)()
            }
            onLeave {
                onLeaveAction()
                (other.onLeaveAction)()
            }
            onExecute {
                (other.onExecuteAction) { (onExecuteAction)(it) }
            }
            onReturn {
                onReturnAction(it)
                (other.onReturnAction)(it)
            }
            onException {
                onExceptionAction(it)
                (other.onExceptionAction)(it)
            }
        }

    /**
     * Combine two aspects with the same context.
     * @param other The other contextualized aspect to combine with this one.
     * @return A new contextualized aspect considering both input aspects.
     */
    operator fun times(other: WithContext<TContext>): WithContext<TContext> =
        WithContext(other.context, this * other.aspect)

    /**
     * An aspect together with a context.
     * @param context The context for the aspect.
     * @param aspect The aspect which is contextualized.
     */
    data class WithContext<TContext>(val context: TContext, val aspect: Aspect<TContext>) {

        /**
         * Execute the given action with the contextualized aspect.
         * @param action The action to execute.
         * @return The return value, either returned by the action or overridden by the aspect.
         */
        operator fun <TResult> invoke(action: () -> TResult): TResult =
            runBlocking {
                @Suppress("UNCHECKED_CAST")
                aspect.callWithInterceptions(context) { action() } as TResult
            }

        /**
         * Execute the given action with the contextualized aspect. This is a convenience method
         * that can be used in case the return value of the action is kotlin.Nothing. This may
         * happen when the return value cannot be inferred because the passed lambda throws
         * an exception.
         * @param action The action to execute.
         * @return The return value overridden by the aspect (if no exception is thrown).
         */
        fun <TResult> nothing(action: () -> Nothing): TResult = invoke<TResult> { action() }

        /**
         * Execute the given suspend action with the contextualized aspect.
         * @param action The action to execute.
         * @return The return value, either returned by the action or overridden by the aspect.
         */
        @Suppress("UNCHECKED_CAST")
        suspend fun <TResult> suspending(action: suspend () -> TResult): TResult =
            aspect.callWithInterceptions(context, action) as TResult

        /**
         * Combine two aspects with the same context.
         * @param other The other aspect to combine with this one.
         * @return A new contextualized aspect considering both input aspects.
         */
        operator fun times(other: Aspect<TContext>): WithContext<TContext> =
            WithContext(context, aspect * other)

        /**
         * Combine two aspects with different contexts.
         * @param other The other contextualized aspect to combine with.
         * @return A new contextualized aspect combining both input aspects.
         */
        operator fun <TOtherContext> plus(
            other: WithContext<TOtherContext>
        ): WithContext<TOtherContext> =
            WithContext(
                other.context,
                Aspect {
                    onEnter {
                        context.(aspect.onEnterAction)()
                        (other.aspect.onEnterAction)()
                    }
                    onLeave {
                        context.(aspect.onLeaveAction)()
                        (other.aspect.onLeaveAction)()
                    }
                    onExecute {
                        (other.aspect.onExecuteAction) { context.(aspect.onExecuteAction)(it) }
                    }
                    onReturn {
                        context.(aspect.onReturnAction)(it)
                        (other.aspect.onReturnAction)(it)
                    }
                    onException {
                        context.(aspect.onExceptionAction)(it)
                        (other.aspect.onExceptionAction)(it)
                    }
                }
            )
    }

    // region Private

    init {
        InitializerImpl().init()
    }

    private class ExceptionContextImpl(override val exception: Throwable) : ExceptionContext {
        private var _returnValue: Any? = null
        val returnValue: Any? get() = _returnValue
        override var isReturningValue = false
        override fun returnWith(returnValue: Any?) {
            isReturningValue = true
            _returnValue = returnValue
        }

        override fun toString() = exception.toString()
    }

    suspend fun <TResult : Any?> callWithInterceptions(
        context: TContext,
        action: suspend () -> TResult
    ): Any? = context.run {
        try {
            execute(action)
        } catch (ex: Throwable) {
            handleException<TResult>(ex)
        } finally {
            onLeaveAction()
        }
    }

    private suspend fun <TResult : Any?> TContext.execute(action: suspend () -> TResult): TResult {
        onEnterAction()
        val result = onExecuteAction { action() }
        @Suppress("UNCHECKED_CAST")
        return result.also { onReturnAction(it) } as TResult
    }

    private suspend fun <TResult : Any?> TContext.handleException(ex: Throwable): Any? {
        val exceptionContext = ExceptionContextImpl(ex)
        onExceptionAction(exceptionContext)
        if (!exceptionContext.isReturningValue)
            throw ex
        @Suppress("UNCHECKED_CAST")
        return exceptionContext.returnValue.also { onReturnAction(it) } as TResult
    }

    private inner class InitializerImpl : Initializer<TContext> {
        override fun onEnter(action: suspend TContext.() -> Unit) {
            onEnterAction = action
        }

        override fun onLeave(action: suspend TContext.() -> Unit) {
            onLeaveAction = action
        }

        override fun onExecute(action: suspend TContext.(toExecute: suspend () -> Any?) -> Any?) {
            onExecuteAction = action
        }

        override fun onReturn(action: suspend TContext.(result: Any?) -> Unit) {
            onReturnAction = action
        }

        override fun onException(action: suspend TContext.(ExceptionContext) -> Unit) {
            onExceptionAction = action
        }
    }

    // endregion
}
