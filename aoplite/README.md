# aop lite 

A kotlin library supports a lightweight way of AOP style programming, without compiler magic, just plain kotlin code.

Aspects may be declared as callable objects, which may execute interception callbacks before and after the execution, as well as on return and on exception. It is also possible to combine multiple aspects sharing the same context or different contexts.

## Example

    var logAspect = MethodCallAspect {
        onEnter { println("entering $call"}
        onLeave { println("leaving $call}"}
    }

    fun doSomething() = logAspect {
        println("Hello World")
    }

See the [Examples](src/test/kotlin/straightway/aoplite/Examples.kt) test for more.  

## Status

This software is in pre-release state. Every aspect of it may change without announcement or
notification or downward compatibility. As soon as version 1.0 is reached, all subsequent
changes for sub versions will be downward compatible. Breaking changes will then only occur
with a new major version with according deprecation marking.

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:aoplite:<version>"
    }
