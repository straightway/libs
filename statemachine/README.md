# statemachine 

A kotlin library providing a generic state machine, which can be configured with custom states, custom events and custom transitions.

## Example

As an example, we assume a state machine which triggers downloading a file from some remote location. The download may be triggered multiple times, but the image shall only be downloaded once. Plus, the download can be cancelled. So we have three states:

* NotDownloaded
* Downloading
* Downloaded

And we have also three events:
* Start download
* Download finished
* Cancel

![An example state machine](StateMachine.png)

We can implement this as follows.

    enum class DownloadState {
        NotDownloaded,
        DownloadPending,
        Downloaded
    }

    enum class DownloadEvents {
        DownloadStart,
        Cancel,
        DownloadFinished
    }

    val stateMachine = StateMachine<DownloadState, DownloadEvents>(
        DownloadState.NotDownloaded // Initial state
    ) {
        inState(DownloadState.NotDownloaded) {
            onEntry {
                cleanUpExistingDownload()
            }
            on(DownloadEvents.DownloadStart) then transitionTo(DownloadState.DownloadPending)
            on(DownloadEvents.Cancel) then stayInThisState
        }
        inState(DownloadState.DownloadPending) {
            onEntry {
                startDownloadJob {
                    downloader.download(basePath, uri)
                    stateMachine.handleEvent(DownloadEvents.DownloadFinished)
                }
            }
            on(DownloadEvents.DownloadStart) then stayInThisState
            on(DownloadEvents.DownloadFinished) then transitionTo(DownloadState.NotDownloaded)
            on(DownloadEvents.Cancel) then transitionTo(DownloadState.Downloaded)
        }
        inState(DownloadState.Downloaded) {
            on(DownloadEvents.DownloadStart) then stayInThisState
            on(DownloadEvents.Cancel) then transitionTo(DownloadState.Downloaded)
        }
    }

The initial state is specified in the StateMachine's constructor. From here on, state transitions can be triggered using the handleEvent method, which can be either called from outside or from inside the state machine.

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:statemachine:<version>"
    }
