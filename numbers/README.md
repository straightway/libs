# numbers

This kotlin library provides extension methods
and operators for calculation with mixed number
types without the necessity to cast. As result
type, always the 'larger' type is used, e.g.
multiplying an Int with a Float results in a Float.

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:numbers:<version>"
    }
