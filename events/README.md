# events 

A kotlin library providing a simple event mechanism. It allows to separate attaching/detaching from firing the events.

## Example

    private val _event = Event<String>()
    val event: EventRegistry<String> get() = _event
    fun fire(s: String) = _event(s)

    val token = event.attach { println(it) }
    fire("hello")
    event.detach(token)

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:events:<version>"
    }
