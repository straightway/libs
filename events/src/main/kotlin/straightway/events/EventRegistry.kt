/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.events

/**
 * Allow attaching to and detaching from an event. To detach, the event token
 * returned by the attach method must be provided to identify the connection.
 */
interface EventRegistry<T> {

    /**
     * Attach a new handler to the event which is called when the event is fired.
     * @param handler The handler to attach to the event.
     * @return A token which can be used to detach the event handler again.
     */
    infix fun attach(handler: (T) -> Unit): EventHandlerToken

    /**
     * Detach a handler from the event, so that it is not called any more when the
     * event is fired.
     * @param token The token of the event handler, that is detached
     * @return True if anything was detached, false if not.
     */
    infix fun detach(token: EventHandlerToken): Boolean
}

fun <T> EventRegistry<T>.handleOnce(handler: (T) -> Unit): EventHandlerToken {
    lateinit var eventHandlerToken: EventHandlerToken
    eventHandlerToken = attach {
        handler(it)
        detach(eventHandlerToken)
    }
    return eventHandlerToken
}
