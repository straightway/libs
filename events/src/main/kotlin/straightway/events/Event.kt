/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.events

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import straightway.error.Panic

/**
 * Event mechanism allowing attaching and detaching handlers as functions or lambdas,
 * and invocation with notification of all attached handlers.
 */
class Event<T> : EventRegistry<T>, EventTrigger<T> {

    override fun invoke(e: T) = runBlocking {
        val handlers = _mutex.withLock {
            if (_isInvocationInProgress) throw Panic("Recursive event invocation")
            _isInvocationInProgress = true
            _handlers.toList()
        }
        try {
            handlers.forEach { it.handler(e) }
        } finally {
            _mutex.withLock {
                _isInvocationInProgress = false
            }
        }
    }

    override infix fun attach(handler: (T) -> Unit): EventHandlerToken = runBlocking {
        val token = EventHandlerToken()
        _mutex.withLock {
            _handlers += HandlerEntry(token, handler)
        }
        token
    }

    override infix fun detach(token: EventHandlerToken) = runBlocking {
        _mutex.withLock {
            _handlers.removeIf { it.token === token }
        }
    }

    // region Private

    private data class HandlerEntry<T>(val token: EventHandlerToken, val handler: (T) -> Unit)
    private val _handlers = mutableListOf<HandlerEntry<T>>()
    private var _isInvocationInProgress = false
    private var _mutex = Mutex()

    // endregion
}
