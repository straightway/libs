/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
rootProject.name = "libs"
include(
    "aoplite",
    "assertions",
    "bdd",
    "binaryconversion",
    "collections",
    "comparison",
    "compiler",
    "crypto",
    "error",
    "events",
    "expr",
    "files",
    "flow",
    "lifecycle",
    "numbers",
    "random",
    "ranges",
    "service",
    "statemachine",
    "stringformat",
    "time",
    "tracing",
    "units")


pluginManagement {
    val propertiesLines =
        rootProject.projectDir.listFiles()!!.single { it.name == "gradle.properties" }.readLines()
    val properties = mapOf(*propertiesLines.filter { !it.startsWith('#') }.map {
        val keyValue = it.split('=')
        keyValue.first() to keyValue.last()
    }.toTypedArray())

    resolutionStrategy.eachPlugin {
        val id = requested.id.id
        if (id.startsWith("org.jetbrains.kotlin."))
            properties["straightway.version.kotlin"]?.also { useVersion(it) }
        else properties["straightway.version.$id"]?.also { useVersion(it) }
    }
}
