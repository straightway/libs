/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.lifecycle

import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import straightway.assertions.*
import straightway.assertions.assertPanics
import straightway.error.Panic
import java.io.Closeable

class Use {

    @Test
    fun `use calls action`() {
        var isActionCalled = false
        null.use {
            isActionCalled = true
        }

        assertThat(isActionCalled is_ true)
    }

    @Test
    fun `use returns action result`() =
        assertThat(null.use { 83 } is_ equal to_ 83)

    @Test
    fun `use does nothing with non-AutoClosable`() {
        var isActionCalled = false
        "hello".use {
            isActionCalled = true
        }

        assertThat(isActionCalled is_ true)
    }

    @Test
    fun `use closes AutoCloseable on regular scope leave`() {
        val autoCloseable = mock<AutoCloseable>()
        autoCloseable.use {
            verify(autoCloseable, never()).close()
        }

        verify(autoCloseable, times(1)).close()
    }

    @Test
    fun `use closes AutoCloseable when directly returning from action`() {
        val autoCloseable = mock<AutoCloseable>()
        assertThat(directReturnFromAction(autoCloseable) is_ equal to_ 83)
        verify(autoCloseable, times(1)).close()
    }

    @Test
    fun `use closes AutoCloseable on scope leave via exception`() {
        val autoCloseable = mock<AutoCloseable>()
        assertPanics {
            autoCloseable.use {
                throw Panic("Aaaah!")
            }
        }

        verify(autoCloseable, times(1)).close()
    }

    @Test
    fun `use closes Closeable on regular scope leave`() {
        val autoCloseable = mock<Closeable>()
        autoCloseable.use {
            verify(autoCloseable, never()).close()
        }

        verify(autoCloseable, times(1)).close()
    }

    @Test
    fun `use closes Closeable when directly returning from action`() {
        val autoCloseable = mock<Closeable>()
        assertThat(directReturnFromAction(autoCloseable) is_ equal to_ 83)
        verify(autoCloseable, times(1)).close()
    }

    @Test
    fun `use closes Closeable on scope leave via exception`() {
        val autoCloseable = mock<Closeable>()
        assertPanics {
            autoCloseable.use {
                throw Panic("Aaaah!")
            }
        }

        verify(autoCloseable, times(1)).close()
    }

    // region Private

    private fun directReturnFromAction(autoClosable: AutoCloseable): Int {
        autoClosable.use {
            return 83
        }
    }

    // endregion
}
