/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.bdd

import org.junit.jupiter.api.fail
import straightway.lifecycle.use

/**
 * Continuation of a given/when/then clause. @see Given
 */
class GivenWhen<TGiven, TResult>(
    val given: TGiven,
    val result: WhenResult<TResult>
) {
    infix fun then(op: TGiven.(TResult) -> Unit): Unit =
        given.use {
            result.result.use {
                result.throwExceptionIfAny()
                executeThenBlock(op)
            }
        }

    infix fun then(cont: Then<TGiven, TResult>) =
        given.use { result.result.use { cont.check(given, result) } }

    infix fun then(cont: ThenUntyped) =
        given.use { result.result.use { cont.check(result) } }

    // region Private

    private fun executeThenBlock(thenBlock: TGiven.(TResult) -> Unit) {
        if (result.result === null) fail("result is null")
        given.thenBlock(result.result)
    }

    // endregion
}
