# bdd

This kotlin library provides support for behaviour driven development style tests (BDD, Given-When-Then schema).

## Example

    @Test
    fun `example for bdd style test`() =
        Given {
            "Hello"
        } when_ {
            this + " world"
        } then {
            assertThat(it is_ equal to "hello world")
        }

It is also possible to extract the 'Given' part to a property and re-use it in several tests. In this case, the property should not store the value, but create a fresh 'Given' object on each call in order to make the tests independent.

## Example

    private val test get() = Given { "Hello" }

    @Test
    fun `example test re-using the given setup`() =
        test when_ {
            this + " world"
        } then {
            assertThat(it is_ equal to "hello world")
        }

    @Test
    fun `another test using the given setup`() =
        test when_ {
            this + " mars"
        } then {
            assertThat(it is_ equal to "hello mars")
        }

To have a tear-down after each test, the object returned by 'Given' must implement the 'java.lang.AutoCloseable' interface.

    private val test get() = Given {
        object : AutoCloseable {
            val sut = "Hello"
            override fun close() {
                println("tear down")
            }
        }
    }

    @Test
    fun `example test with tear down`() =
        test when_ {
            sut + " world"
        } then {
            assertThat(it is_ equal to "hello world")
        } // Tear down happens here

The last example also shows a more complex setup with a test object. This object may even have an anonymous type (as in the example), if the 'test' property is private.

Sometimes, test require specific additional setup. This can be achieved the following way:

    private val test get() = Given {
        mutableListOf<String>()
    }

    @Test
    fun `example test with additional setup`() =
        test while_ {
            add("hello")
            add("world")
        } when_ {
            joinToString(" ")
        } then {
            assertThat(it is_ equal to "hello world")
        }

The result in 'then' is not nullable. Null results have a special syntax:

    @Test
    fun `example for test checking for null`() =
        Given {
            "Hello"
        } when_ {
            null
        } then isNull

The 'isNull' may be followed by an optional block to check more post conditions.

Also expected exceptions are handled differently:

    @Test
    fun `example for test checking exception`() =
        Given {
            { throw ArrayStoreException() }
        } when_ {
            this()
        } then throws.exception {
            assertThat(it is ArrayStoreException)
        }

## Include in gradle builds

To include this library in a gradle build, add

    repositories {
        ...
        maven { url "https://pages.codeberg.org/straightway/repo" }
    }

Then you can simply configure in your dependencies:

    dependencies {
        compile "straightway:bdd:<version>"
    }
