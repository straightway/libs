/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.flow

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Timeout
import straightway.assertions.*
import straightway.bdd.Given
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import kotlin.time.Duration.Companion.milliseconds

class WORMFlowTest {

    @Test
    @Timeout(15, unit = TimeUnit.SECONDS)
    fun `empty base flow leads to empty WORMFlow`() = runBlocking {
        coroutineScope {
            Given {
                wormFlowOf(flowOf<String>())
            } whenBlocking  {
                toList()
            } then {
                assertThat(it is_ empty)
            }
        }
    }

    @Test
    @Timeout(15, unit = TimeUnit.SECONDS)
    fun `filled base flow is transferred by WORMFlow`() = runBlocking {
        coroutineScope {
            Given {
                wormFlowOf(flowOf("a", "b", "c"))
            } whenBlocking  {
                toList()
            } then {
                assertThat(it is_ equal to listOf("a", "b", "c"))
            }
        }
    }

    @Test
    @Timeout(15, unit = TimeUnit.SECONDS)
    fun `partly collected flow works properly`() = runBlocking {
        coroutineScope {
            Given {
                wormFlowOf(flowOf("a", "b", "c"))
            } whenBlocking  {
                take(1).toList()
            } then {
                assertThat(it is_ equal to listOf("a"))
            }
        }
    }

    @Test
    @Timeout(15, unit = TimeUnit.SECONDS)
    fun `multiple receivers`() = runBlocking {
        val counter = AtomicInteger()
        coroutineScope {
            Given {
                wormFlowOf(
                    flow {
                        emit(counter.incrementAndGet())
                        emit(counter.incrementAndGet())
                        emit(counter.incrementAndGet())
                    }
                )
            } whenBlocking  {
                listOf(toList(), toList())
            } then {
                assertThat(it.first() is_ equal to listOf(1, 2, 3))
                assertThat(it.last() is_ equal to listOf(1, 2, 3))

                // Base flow is only evaluated once
                assertThat(counter.get() is_ equal to 3)
            }
        }
    }

    @Test
    @Timeout(15, unit = TimeUnit.SECONDS)
    fun `sender slower than receiver`() = runBlocking {
        coroutineScope {
            val valuesChannel = Channel<Int>()
            val sut = wormFlowOf(valuesChannel.receiveAsFlow())
            launch {
                delay(20.milliseconds)
                valuesChannel.send(1)
                delay(20.milliseconds)
                valuesChannel.send(2)
                delay(20.milliseconds)
                valuesChannel.send(3)
                delay(20.milliseconds)
                valuesChannel.close()
            }

            assertThat(sut.toList() is_ equal to listOf(1, 2, 3))
        }
    }

    @Test
    @Timeout(15, unit = TimeUnit.SECONDS)
    fun `receiver slower than sender`() = runBlocking {
        coroutineScope {
            val valuesChannel = Channel<Int>()
            val sut = wormFlowOf(valuesChannel.receiveAsFlow())
            launch {
                valuesChannel.send(1)
                valuesChannel.send(2)
                valuesChannel.send(3)
                valuesChannel.close()
            }

            val result = sut.map { delay(20.milliseconds); it }.toList()
            assertThat(result is_ equal to listOf(1, 2, 3))
        }
    }

    @Test
    @Timeout(15, unit = TimeUnit.SECONDS)
    fun `one receiver slower and one faster than sender`() = runBlocking {
        coroutineScope {
            val valuesChannel = Channel<Int>()
            val sut = wormFlowOf(valuesChannel.receiveAsFlow())
            launch {
                delay(10.milliseconds)
                valuesChannel.send(1)
                delay(10.milliseconds)
                valuesChannel.send(2)
                delay(10.milliseconds)
                valuesChannel.send(3)
                delay(10.milliseconds)
                valuesChannel.close()
            }

            val result1 = async { sut.map { delay(20.milliseconds); it }.toList() }
            val result2 = async { sut.toList() }

            assertThat(result1.await() is_ equal to listOf(1, 2, 3))
            assertThat(result2.await() is_ equal to listOf(1, 2, 3))
        }
    }
}