/*
 * Copyright 2016 straightway contributors
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.flow

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.AbstractFlow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableSharedFlow
import java.util.concurrent.atomic.AtomicReference

/**
 * Creates a new Write One Read Many flow.
 * Takes a coroutine scope as receiver, which is used to start the collection of the input
 * flow in the background.
 * @param input The input flow, which yields the values forwarded by the WORMFlow. It is
 * only collected once, and its yielded values are buffered for an arbitrary number of
 * collectors of the WORMFlow.
 */
fun <T> CoroutineScope.wormFlowOf(input: Flow<T>): Flow<T> = WORMFlow(input, this)

/**
 * A Write One Read Many flow.
 *
 * It takes an input flow and collects it exactly once. Nevertheless, the values can be
 * read by an arbitrary number of collectors.
 */
@OptIn(FlowPreview::class)
private class WORMFlow<T>(
    private val input: Flow<T>,
    coroutineScope: CoroutineScope
) : AbstractFlow<T>() {

    override suspend fun collectSafely(collector: FlowCollector<T>) {
        try {
            var currItem: Node<T> = firstNode
            var lastEmittedItem: Node<T>? = null
            events.collect {
                while (true) {
                    if (lastEmittedItem !== currItem) {
                        lastEmittedItem = currItem
                        currItem.emitOn(collector)
                    }

                    if (!currItem.hasNext)
                        break

                    currItem = currItem.next
                }
            }
        } catch (_: CancelException) {
            // Do nothing
        }
    }

    private class CancelException : Exception("WORMFlow collection cancelled", null, false, false)
    private val events = MutableSharedFlow<Unit>(replay = Int.MAX_VALUE)
    private val firstNode: Node<T> = SuccessableNode()
    private var lastNode: Node<T> = firstNode

    private interface Node<T> {
        suspend fun emitOn(collector: FlowCollector<T>)
        val hasNext: Boolean
        var next: Node<T>
    }

    private open class SuccessableNode<T> : Node<T> {
        private val nextRef = AtomicReference<Node<T>>()
        override suspend fun emitOn(collector: FlowCollector<T>) { /* do nothing */ }
        override val hasNext: Boolean get() = nextRef.get() !== null
        final override var next: Node<T>
            get() = nextRef.get()!!
            set(value) { nextRef.set(value) }
    }

    private class ValueNode<T>(val item: T) : SuccessableNode<T>() {
        override suspend fun emitOn(collector: FlowCollector<T>) {
            collector.emit(item)
        }
    }

    private class FinalNode<T> : Node<T> {
        override suspend fun emitOn(collector: FlowCollector<T>) = throw CancelException()
        override val hasNext: Boolean = false
        override lateinit var next: Node<T>
    }

    init {
        coroutineScope.launch {
            input.collect {
                appendItem(ValueNode(it))
            }
            appendItem(FinalNode())
        }
    }

    private suspend fun appendItem(node: Node<T>) {
        lastNode.next = node
        lastNode = node
        events.emit(Unit)
    }
}
